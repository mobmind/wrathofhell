package com.mobmind.woh;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mobmind.woh.Actors.MobActor;

public class DamageText {
	public DamageText(String damage, MobActor mob, Skin skin){
		Stage stage = Kernel.getInstance().getStage();

		Label text = new Label(damage, skin);
		text.setPosition(mob.getX() + mob.getOriginX(), mob.getY() + mob.getOriginY());

		text.addAction(Actions.sequence(
			Actions.parallel(
					Actions.moveTo(text.getX(), text.getY() + mob.getOriginX(), 3f),
					Actions.fadeOut(3f)
			),
			Actions.removeActor()
		));

		stage.addActor(text);
	}
}
