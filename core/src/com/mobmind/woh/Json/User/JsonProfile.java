package com.mobmind.woh.Json.User;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.mobmind.woh.Kernel;

/**
 * Created by seedofpanic on 20.07.2014.
 */
public class JsonProfile {
    public String name;
    public Array<JsonPlayedLevel> playedLevels;
    public boolean soundOn = true;
    public boolean musicOn = true;
    public ArrayMap<String, Integer> skillsLevels;

    @Override
    public String toString() {
        return name;
    }

    public JsonPlayedLevel getPlayedLevelByName(String circleName, String levelName) {
        if (playedLevels != null) {
            for (JsonPlayedLevel playedLevel : playedLevels) {
                if (playedLevel.equals(circleName, levelName)) {
                    return playedLevel;
                }
            }
        }
        return null;
    }

    public int setLevelProgress(String levelName, boolean completed, int totalKilled, long totalScore) {
        if (playedLevels == null)
        {
            playedLevels = new Array<JsonPlayedLevel>();
        }
        JsonPlayedLevel playedLevel = findLevelByName(levelName);
        if (playedLevel == null) {
            playedLevel = new JsonPlayedLevel();
            playedLevel.name = levelName;
            playedLevels.add(playedLevel);
        }
        playedLevel.compleated = completed;
        playedLevel.highKilled = Math.max(totalKilled, playedLevel.highKilled);
        playedLevel.highScore = Math.max(totalScore, playedLevel.highScore);
        if (playedLevel.stars < 3)
        {
            playedLevel.stars++;
        }
        return playedLevel.stars;
    }

    private JsonPlayedLevel findLevelByName(String levelName) {
        for (JsonPlayedLevel level : playedLevels)
        {
            if (level.name.equals(levelName))
            {
                return level;
            }
        }
        return null;
    }
}
