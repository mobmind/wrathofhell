package com.mobmind.woh.Json.User;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;

import java.util.Iterator;

/**
 * Created by seedofpanic on 21.07.2014.
 */
public class JsonProfiles {
    Array<JsonProfile> profiles;
    String curProfile;

    public JsonProfile getCurrent() {

        JsonProfile profile = seekForProfile(curProfile);
        if (profile == null) {
            if (profiles.size > 0) {
                profile = profiles.first();
            }
        }
        return profile;
    }

    private JsonProfile seekForProfile(String name)
    {
        if (profiles != null) {
            for (Iterator<JsonProfile> i = profiles.iterator(); i.hasNext(); ) {
                JsonProfile profile = i.next();
                if (profile.name.equals(name)) {
                    return profile;
                }
            }
        }
        return null;
    }

    public JsonProfile addProfile(String name)
    {
        if (profiles == null){ profiles = new Array<JsonProfile>(); }
        JsonProfile newProfile = new JsonProfile();
        newProfile.name = name;
        profiles.add(newProfile);
        return newProfile;
    }

    public void deleteProfile(JsonProfile profile)
    {
        for (int i = 0; i < profiles.size; i++) {
            if (profiles.get(i).name.equals(profile.name)) {
                profiles.removeIndex(i);
                return;
            }
        }
    }

    public Array<JsonProfile> getProfiles() {
        return profiles;
    }

    public JsonProfile setCurProfile(String name) throws Exception {
        JsonProfile profile = seekForProfile(name);
        if (profile != null)
        {
            curProfile = profile.name;
            return profile;
        }
        throw new Exception("There is no profile with such name " + name);
    }

    public boolean profileExists(String name) {
        return seekForProfile(name) != null;
    }
}
