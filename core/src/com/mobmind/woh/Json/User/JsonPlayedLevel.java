package com.mobmind.woh.Json.User;

/**
 * Created by seedofpanic on 20.07.2014.
 */
public class JsonPlayedLevel {
    public String name;
    public boolean compleated;
    public int stars = 0;
    public int highKilled = 0;
    public long highScore = 0;

    //Возможно стоит проверять круг тоже, но будем считать, что имена уникальны
    public boolean equals(String circleName, String levelName) {
        return name.equals(levelName);
    }

    public boolean isCompleated() {
        return compleated;
    }
}
