package com.mobmind.woh.Json.Spawn;

/**
 * Created by seedofpanic on 06.08.2014.
 */
public abstract class JsonWaveAction {
    public float time;
    public abstract float act();

    public abstract boolean isFinished();

    public void init()
    {

    }
}
