package com.mobmind.woh.Json.Spawn;

import com.badlogic.gdx.utils.Array;

public class JsonLevelData {
	public String briefing;
	public Array<JsonWaves> waves;
}
