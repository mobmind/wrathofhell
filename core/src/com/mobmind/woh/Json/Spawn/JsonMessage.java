package com.mobmind.woh.Json.Spawn;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Tools.Preloader;


/**
 * Created by seedofpanic on 06.08.2014.
 */
public class JsonMessage extends JsonWaveAction{

    String text;
    float timeToShow;
    boolean finished = false;
    MessageLevel type;

    @Override
    public float act() {
        finished = true;
        new GameMessage();
        return timeToShow;
    }

    @Override
    public boolean isFinished() {
        return finished;
    }

    enum MessageLevel{
        Common, Alert
    }

    class GameMessage extends Actor
    {
        private final BitmapFont font;
        Color color;
        private float stateTime = 0;
        public float time;
        boolean fade = false;

        GameMessage()
        {
            Kernel core = Kernel.getInstance();
            Stage stage = core.getStageUI();
            font = Preloader.mainFont30;
            BitmapFont.TextBounds bounds = font.getBounds(text);
            setPosition(stage.getWidth() / 2 - bounds.width / 2, stage.getHeight() / 2);
            switch (type)
            {
                case Common:
                    color = Color.GREEN.cpy();
                    break;
                case Alert:
                    color = Color.RED.cpy();
                    break;
                default:
                    color = Color.BLACK.cpy();
            }
            color.a = 0;
            time = (timeToShow > 0 ? timeToShow : 5) / 2;
            stage.addActor(this);
        }

        @Override
        public void act(float delta) {
            if (fade)
            {
                stateTime -= delta;
                float shift = stateTime / time;
                if (shift <= 0) {
                    remove();
                }
                color.a = Interpolation.pow4.apply(shift);
            }else {
                stateTime += delta;
                float shift = stateTime / time;
                if (shift >= 1) {
                    shift = 1;
                    fade = true;
                }
                color.a = Interpolation.pow4.apply(shift);
            }
        }

        @Override
        public void draw(Batch batch, float parentAlpha) {
            font.setColor(color);
            font.draw(batch, text, getX(), getY());
        }
    }
}
