package com.mobmind.woh.Json.Spawn;

import com.mobmind.woh.Actors.MobActor;
import com.mobmind.woh.Tools.Difficulty;
import com.mobmind.woh.Tools.Pool;

public class JsonGroup extends JsonWaveAction {
	public String type;
	public int count;
    public float mobTime = 0;

    @Override
    public float act() {
	    MobActor mob = Pool.getInstance().getMobs().obtain();
	    mob.setType(type);
        count--;
        if (count > 0) {
            return mobTime;
        }else{
            return 0;
        }
    }

    @Override
    public void init() {
        Difficulty.modify(this);
        super.init();
    }

    @Override
    public boolean isFinished() {
        return count <= 0;
    }
}
