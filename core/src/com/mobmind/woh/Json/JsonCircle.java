package com.mobmind.woh.Json;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Tools.Preloader;

/**
 * Created by seedofpanic on 26.07.2014.
 */
public class JsonCircle extends Actor {
    private Texture image;
    private Texture circleBack;

    //json
    String name;
    String file;
    boolean blocked;
    int width;
    int height;
    Array<JsonLevel> levels;

    public Array<JsonLevel> getLevels()
    {
        return levels;
    }

    public JsonCircle()
    {
        Kernel core = Kernel.getInstance();
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(image, getX(), getY(), getOriginX(), getOriginY(), width, height, getScaleX(), getScaleY(), 0, 0, 0, width, height, false, false);
        batch.draw(circleBack, getX() + 25 * getScaleX(), getY() + 10 * getScaleY(), getOriginX(), getOriginY(), width, height, getScaleX(), getScaleY(), 0, 0, 0, width, height, false, false);
    }

    public void init(Group parent) {
        Kernel core = Kernel.getInstance();
        circleBack = Preloader.manager.get(Preloader.ringOfMenu);
        if (blocked)
        {
            image = Preloader.manager.get(Preloader.circleClosed);
        }else {
            image = Preloader.manager.get(Preloader.circle1);
        }
        width = image.getWidth();
        height = image.getHeight();
        setOrigin(width / 2, height / 2);
        parent.addActor(this);
    }

    @Override
    public float getWidth() {
        return width;
    }

    @Override
    public float getHeight() {
        return height;
    }

    public boolean isBlocked() {
        return blocked;
    }
}
