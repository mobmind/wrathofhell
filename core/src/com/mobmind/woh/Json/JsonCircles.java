package com.mobmind.woh.Json;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;

/**
 * Created by seedofpanic on 26.07.2014.
 */
public class JsonCircles {
    Array<JsonCircle> circles;
    public void init(Group parent) {
        for (JsonCircle circle : circles) {
            circle.init(parent);
        }
    }
}
