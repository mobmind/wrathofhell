package com.mobmind.woh.Json.Serializers;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.mobmind.woh.Json.Spawn.JsonWaveAction;

/**
 * Created by seedofpanic on 06.08.2014.
 */
public class JsonGroupsSerializer implements Json.Serializer<JsonWaveAction> {

    @Override
    public void write(Json json, JsonWaveAction object, Class knownType) {

    }

    @Override
    public JsonWaveAction read(Json json, JsonValue jsonData, Class type) {
        char[] chars = jsonData.get("data").toString().toCharArray();
        try {
            return (JsonWaveAction)json.fromJson(Class.forName("com.mobmind.woh.Json.Spawn.Json" + jsonData.getString("type")), chars, 0, chars.length);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
