package com.mobmind.woh.Json;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.mobmind.woh.Actors.Menu.AnimatedActor;
import com.mobmind.woh.Json.User.JsonPlayedLevel;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Tools.AnimButton;
import com.mobmind.woh.Tools.Difficulty;
import com.mobmind.woh.Tools.Preloader;

/**
 * Created by seedofpanic on 28.07.2014.
 */
public class JsonLevel extends Group {
    //Texture background;
    AnimButton numberActor;
    int number;
    private String file;
    private Texture background;
    private AnimatedActor star1;
    private AnimatedActor star2;
    private AnimatedActor star3;
    private int showStars;
    private boolean completed = false;

    private int[][] starsCoords = {
            {35, 61, 101, 32, 167, 62},
            {36, 64, 102, 35, 168, 65},
            {36, 64, 102, 35, 168, 65},
            {35, 64, 101, 35, 167, 65},
            {36, 64, 102, 35, 168, 65},
            {36, 64, 102, 35, 168, 65},
            {35, 64, 101, 35, 167, 65},
            {36, 64, 102, 35, 168, 65},
            {36, 64, 102, 35, 168, 65},
    };
    private Texture numberTexture;

    public JsonLevel()
    {
        final Kernel core = Kernel.getInstance();
        //background = core.getPreloader().pauseList;


    }

    public void init(float x, float y, boolean available, JsonPlayedLevel playedLevel, Animation star, Animation emptyStar)
    {
        final Kernel core = Kernel.getInstance();

        if (!(number > 0))
        {
            number = 1;
        }

        showStars = 0;
        completed = false;

        if (available) {
            clear();
            background = Preloader.manager.get(Preloader.sublevels.get(number - 1));
            setBounds(x, y, background.getWidth(), background.getHeight());
            addListener(new InputListener() {
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    ((InputListener) numberActor.getListeners().first()).touchDown(event, x, y, pointer, button);
                    return true;
                }
            });
            numberTexture = Preloader.manager.get(Preloader.numbers.get(number - 1));
            numberActor = new AnimButton(numberTexture, 0, 0, numberTexture.getWidth(), 70, Preloader.textKeyFrames, 0.04f);
            numberActor.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    core.setCurrentLevel(file);
                    Difficulty.clear();
                    if (showStars == 1)
                    {
                        Difficulty.setDoubledNumber(true);
                    }
                    if (showStars > 1)
                    {
                        Difficulty.setDoubledHelth(true);
                        Difficulty.setDoubledNumber(true);
                    }
                    core.showScreen(core.getGameScreen());
                }
            });
            numberActor.setPosition(122 - numberActor.getWidth() / 2, 110);
            numberActor.setSoundEffect(Preloader.manager.get(Preloader.sound_ignite));
            addActor(numberActor);


            if (playedLevel != null && playedLevel.compleated)
            {
                showStars = playedLevel.stars;
                completed = true;
            }

            star1 = new AnimatedActor(showStars > 0 ? star : emptyStar, true);
            star1.setPosition(starsCoords[number - 1][0], starsCoords[number - 1][1]);
            star2 = new AnimatedActor(showStars > 1 ? star : emptyStar, true);
            star2.setPosition(starsCoords[number - 1][2], starsCoords[number - 1][3]);
            star3 = new AnimatedActor(showStars > 2 ? star : emptyStar, true);
            star3.setPosition(starsCoords[number - 1][4], starsCoords[number - 1][5]);
            addActor(star1);
            addActor(star2);
            addActor(star3);

        }else{
            setPosition(x - 8, y + 50);
            background = Preloader.manager.get(Preloader.planks.get(number - 1));
            clearListeners();

        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(background, getX(), getY());
        super.draw(batch, parentAlpha);
    }
}
