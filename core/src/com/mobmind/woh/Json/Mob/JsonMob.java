package com.mobmind.woh.Json.Mob;

import com.mobmind.woh.Json.Mob.Anim.JsonAnim;
import com.mobmind.woh.Json.Mob.Stats.JsonStats;
import com.mobmind.woh.Tools.Mob;

public class JsonMob {
	public JsonAnim anim;
	public JsonStats stats;
    public Mob.Type texture;
    public String AI;
}
