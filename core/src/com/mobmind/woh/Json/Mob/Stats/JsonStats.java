package com.mobmind.woh.Json.Mob.Stats;

import com.badlogic.gdx.utils.Array;
import com.mobmind.woh.Json.Mob.Stats.Resist.JsonResist;

public class JsonStats {
	public int HP;
	public Array<JsonResist> resist;
    public float speed;
    public float damage;
    public float attackSpeed;
}
