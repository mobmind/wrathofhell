package com.mobmind.woh.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.mobmind.woh.Actors.TextActor;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Tools.AnimButton;
import com.mobmind.woh.Tools.Preloader;

/**
 * Created by seedofpanic on 23.07.2014.
 */
public class Dialog extends Group{
    private final TextActor msgText;
    NinePatch background;
    private float bgX;
    private float bgY;
    private float bgW;
    private float bgH;
    private AnimButton yesBtn;
    private AnimButton noBtn;

    public Dialog(ChangeListener yesLogic, ChangeListener noLogic)
    {
        Kernel core = Kernel.getInstance();
        msgText = new TextActor(new BitmapFontCache(Preloader.mainFont22));
        msgText.getFont().setColor(Color.BLACK);

        background = new NinePatch(Preloader.manager.get(Preloader.window9p), 37, 37, 37, 37);


        msgText.setText("", 0, 0, true, false);

        addActor(msgText);
        yesBtn = new AnimButton(Preloader.manager.get(Preloader.yesBtn), 0, 0, 148, 60, core.getMenuScreen().animBtnKeyFrames, 0.04f);
        yesBtn.addListener(yesLogic);
        yesBtn.setSoundEffect(Preloader.manager.get(Preloader.sound_ignite));
        addActor(yesBtn);
        noBtn = new AnimButton(Preloader.manager.get(Preloader.noBtn), 0, 0, 135, 60, core.getMenuScreen().animBtnKeyFrames, 0.04f);
        noBtn.addListener(noLogic);
        noBtn.setSoundEffect(Preloader.manager.get(Preloader.sound_ignite));
        addActor(noBtn);


    }

    @Override
    public void setVisible(boolean visible) {
        Kernel.getInstance().getDialogBack().setVisible(visible);
        toFront();
        super.setVisible(visible);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        background.draw(batch, bgX, bgY, bgW, bgH);
        super.draw(batch, parentAlpha);
    }

    public void setMsg(String msg)
    {
        msgText.getFont().setWrappedText(msg, -400, 70, 800, BitmapFont.HAlignment.CENTER);
        yesBtn.setPosition(- 400, - 100);
        noBtn.setPosition(400 - noBtn.getWidth(), - 100);

        bgW = 900;
        bgH = 270;
        bgX = getX() - bgW / 2;
        bgY = getY() - bgH / 2;
    }
}
