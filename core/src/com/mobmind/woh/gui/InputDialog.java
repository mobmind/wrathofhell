package com.mobmind.woh.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.*;
import com.esotericsoftware.tablelayout.Cell;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Tools.AnimButton;
import com.mobmind.woh.Tools.Pallet;
import com.mobmind.woh.Tools.Preloader;


/**
 * Created by seedofpanic on 23.07.2014.
 */
public class InputDialog extends Table {
    private final Label msgText;
    private final Cell<TextField> cell;
    private final AnimButton cancelBtn;
    private final Cell<AnimButton> okCell;
    NinePatch background;
    private float bgX;
    private float bgY;
    private float bgW;
    private float bgH;
    private AnimButton okBtn;
    private TextField inputText;
    private char[] allowdChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_".toCharArray();
    private Cell<Actor> cancelCell;

    public InputDialog(String msg, ChangeListener okLogic)
    {
        Kernel core = Kernel.getInstance();
        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.fontColor = Pallet.Brown;
        labelStyle.font = Preloader.mainFont22;
        msgText = new Label(msg, labelStyle);
        //msgText.setWrap(true);
        msgText.setAlignment(Align.center);

        setBackground(new NinePatchDrawable(new NinePatch(Preloader.manager.get(Preloader.window9p), 37, 37, 37, 37)));
        align(Align.center);
        add(msgText).padBottom(0).colspan(2).row();
        layout();

        TextField.TextFieldStyle style = new TextField.TextFieldStyle();
        style.background = new BaseDrawable();
        style.font = Preloader.mainFont30;
        style.fontColor = Color.BLACK;
        style.messageFont = Preloader.mainFont30;
        style.messageFontColor = Color.BLACK.cpy();
        style.messageFontColor.a = 1f;
        style.selection = new TextureRegionDrawable(new TextureRegion(Preloader.manager.get(Preloader.dialogBack)));
        inputText = new TextField("", style);
        inputText.setMessageText("New Profile");

        inputText.setTextFieldListener(new TextField.TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                update();
            }
        });
        inputText.setTextFieldFilter(new TextField.TextFieldFilter() {
            @Override
            public boolean acceptChar(TextField textField, char c) {
                for (char ac : allowdChars) {
                    if (c == ac)
                    {
                        return true;
                    }
                }
                return false;
            }
        });
        inputText.setMaxLength(11);

        cell = add(inputText).colspan(2);
        cell.padBottom(10).row();
        cell.width(Preloader.mainFont30.getBounds("New Profile").width);
        layout();


        okBtn = new AnimButton(Preloader.manager.get(Preloader.okBtn), 0, 0, 133, 60, core.getMenuScreen().animBtnKeyFrames, 0.04f);
        okBtn.addListener(okLogic);
        okCell = add(okBtn).padBottom(20);
        okBtn.setSoundEffect(Preloader.manager.get(Preloader.sound_ignite));
        cancelCell = add().padBottom(20);
        cancelBtn = new AnimButton(Preloader.manager.get(Preloader.cancelBtn), 0, 0, 274, 60, core.getMenuScreen().animBtnKeyFrames, 0.04f);
        cancelBtn.setSoundEffect(Preloader.manager.get(Preloader.sound_ignite));
        cancelBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                setVisible(false);
            }
        });

        setWidth(900);
        setHeight(getPrefHeight());
    }

    @Override
    public void setVisible(boolean visible) {
        Kernel.getInstance().getDialogBack().setVisible(visible);
        toFront();
        super.setVisible(visible);
        Stage stage = getStage();
        if (stage != null){
            stage.setKeyboardFocus(inputText);
        }
        inputText.getOnscreenKeyboard().show(visible);
    }

    public void setInputText(String text)
    {
        inputText.setText(text);
        update();
    }

    private void update()
    {
        if (inputText.getText().length() > 0) {
            cell.width(Preloader.mainFont30.getBounds(inputText.getText()).width);
        }else{
            cell.width(Preloader.mainFont30.getBounds("New Profile").width);
        }
        layout();
    }

    public TextField getInputText() {
        return inputText;
    }

    public void setCloseable(boolean closeable) {
        if (closeable)
        {
            okCell.colspan(1);
            cancelCell.setWidget(cancelBtn);
            layout();
        }else{
            okCell.colspan(2);
            cancelCell.setWidget(null);
            layout();
        }
    }

    @Override
    public void setPosition(float x, float y) {
        x -= getWidth() / 2;
        y -= getHeight() / 2;
        super.setPosition(x, y);
    }
}
