package com.mobmind.woh.gui;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Disableable;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Array;
import com.mobmind.woh.Tools.Preloader;


/**
 * Created by seedofpanic on 23.07.2014.
 */
public class ListBox<T> extends WidgetGroup implements Disableable {
    private List list;
    private ScrollPane scrollPane;
    private boolean disabled;

    public ListBox(ListBoxStyle style)
    {
        style.scrollPaneStyle.background = new NinePatchDrawable(new NinePatch(Preloader.manager.get(Preloader.window9p), 37, 37, 37, 37));
        list = new List<T>(style.listStyle);

        scrollPane = new ScrollPane(list, style.scrollPaneStyle);
        scrollPane.setFadeScrollBars(false);
        //Kernel.getInstance().getStage().addActor(scrollPane);

        addActor(scrollPane);

    }

    @Override
    public void setBounds(float x, float y, float width, float height) {
        list.setBounds(0, 0, width, height);
        scrollPane.setBounds(0, 0, width, height);
        super.setBounds(x, y, width, height);
    }

    public void addChangListener(ChangeListener listener)
    {
        list.addListener(listener);
    }

    public void setItems(Array<T> items, T selected)
    {
        list.setItems(items);
        for (int i = 0; i < items.size; i++) {
            if (items.get(i).toString().equals(selected.toString())) {
                list.setSelectedIndex(i);
                break;
            }
        }
    }

    @Override
    public void setDisabled(boolean isDisabled) {
        disabled = isDisabled;
    }

    public static class ListBoxStyle{
        public List.ListStyle listStyle;
        public ScrollPane.ScrollPaneStyle scrollPaneStyle;
    }
}
