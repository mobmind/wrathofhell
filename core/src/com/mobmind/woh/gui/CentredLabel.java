package com.mobmind.woh.gui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

/**
 * Created by seedofpanic on 15.08.2014.
 * Класс реализует надпись центруемую по середине своих координат при изменении текста
 */
public class CentredLabel extends Label {
    float x;
    float y;
    boolean centerY = false;

    public CentredLabel(String s, LabelStyle labelStyle) {
        super(s, labelStyle);
    }

    @Override
    public void setText(CharSequence newText) {
        super.setText(newText);
        BitmapFont.TextBounds bounds = getBitmapFontCache().getFont().getBounds(newText);
        if (centerY) {
            super.setBounds(x - bounds.width / 2, y - bounds.height / 2, bounds.width, bounds.height);
        }else{
            super.setBounds(x - bounds.width / 2, y, bounds.width, bounds.height);
        }
    }

    @Override
    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
        super.setPosition(x, y);
        setText(getText()); // Обновим центрование
    }

    public void setCenterY(boolean centerY)
    {
        this.centerY = centerY;
    }
}
