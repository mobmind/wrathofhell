package com.mobmind.woh.gui;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.mobmind.woh.Actors.Menu.ModalGroup;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Tools.Preloader;
import com.mobmind.woh.Tools.SettingsMenu;

/**
 * Created by seedofpanic on 10.08.2014.
 */
public class ControlDialog extends ModalGroup {
    public ControlDialog()
    {
        final Kernel core = Kernel.getInstance();
        Stage stageUI = core.getStageUI();
        Table table = new Table();
        addActor(table);
        table.setWidth(938);
        table.setHeight(380);
        table.setPosition(stageUI.getWidth() / 2 - table.getWidth() / 2, stageUI.getHeight() / 2 - table.getHeight() / 2);
        table.pad(50);
        table.setBackground(new TextureRegionDrawable(new TextureRegion(Preloader.manager.get(Preloader.pauseDialogBack))));

        Button.ButtonStyle backButtonStyle = new Button.ButtonStyle();
        backButtonStyle.up = new TextureRegionDrawable(new TextureRegion(Preloader.manager.get(Preloader.icon_home)));
        Button backButton = new Button(backButtonStyle);
        backButton.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                core.playSound(Preloader.manager.get(Preloader.sound_click));
                core.showScreen(core.getLevelSelectScreen());
                return true;
            }
        });
        table.add(backButton).width(backButton.getWidth()).height(backButton.getHeight()).padRight(30);

        Button.ButtonStyle replayButtonStyle = new Button.ButtonStyle();
        replayButtonStyle.up = new TextureRegionDrawable(new TextureRegion(Preloader.manager.get(Preloader.icon_restart1)));
        Button replayButton = new Button(replayButtonStyle);
        replayButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.playSound(Preloader.manager.get(Preloader.sound_click));
                core.getGameScreen().refresh();
            }
        });
        table.add(replayButton).width(replayButton.getWidth()).height(replayButton.getHeight()).padRight(30);

        SettingsMenu settingsMenu = new SettingsMenu(0, 0, this);
        Button inMenu = settingsMenu.getButton();
        table.add(inMenu).width(inMenu.getWidth()).height(inMenu.getHeight()).padRight(30);

        ImageButton.ImageButtonStyle menuCloserStyle = new ImageButton.ImageButtonStyle();
        menuCloserStyle.imageUp = new SpriteDrawable(new Sprite(Preloader.manager.get(Preloader.icon_play)));
        ImageButton menuCloser = new ImageButton(menuCloserStyle);
        menuCloser.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.playSound(Preloader.manager.get(Preloader.sound_click));
                setVisible(false);
                core.getGameScreen().unpause();
            }
        });
        table.add(menuCloser).padRight(20);

        setVisible(false);
    }
}
