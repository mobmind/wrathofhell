package com.mobmind.woh.gui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFontCache;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.mobmind.woh.Json.JsonCircle;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Screens.LevelSelectScreen;
import com.mobmind.woh.Tools.Pallet;
import com.mobmind.woh.Tools.Preloader;

import java.util.EventListener;

/**
 * Created by seedofpanic on 26.07.2014.
 */
public class CirclesMetro extends Group implements EventListener {
    private int size;
    private float posX1;
    private float posY1;
    private float posX2;
    private float posY2;
    private float posX3;
    private float posY3;
    //public Array<Actor> slides;
    private float dPos;
    private float dx;
    private float posX0;
    private float posY0;
    private float posX4;
    private float posY4;

    private Texture rama;
    private CentredLabel circleNameLabel;
    private Kernel core;
    private float ramaX;
    private float ramaY;
    private float titleX;
    private float titleY;
    private CharSequence title;

    private Actor child1;
    private Actor child2;
    private Actor child3;
    private Actor child4;
    private Actor child5;



    public CirclesMetro()
    {
        core = Kernel.getInstance();

        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = Preloader.mainFont30;
        labelStyle.fontColor = Pallet.Brown;
        circleNameLabel = new CentredLabel("", labelStyle);
        circleNameLabel.setCenterY(true);

        rama = Preloader.manager.get(Preloader.rama);
        final CirclesMetro me = this;
        addListener(new DragListener(){
            float oldX;
            float oldY;
            float dragging = 0;
            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                dragging += Math.abs(oldX - x);
                me.sweep(x - oldX, y - oldY);
                oldX = x;
                oldY = y;
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                oldX = x;
                oldY = y;
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (dragging > 20) {
                    me.sweepToClosest();
                    dragging = 0;
                }else{
                    Actor actor = me.checkHit(x, y, true);
                    if (actor.getClass() == JsonCircle.class)
                    {
                        if (!((JsonCircle)actor).isBlocked())
                        {
                            Kernel core = Kernel.getInstance();
                            LevelSelectScreen levelSelectScreen = core.getLevelSelectScreen();
                            levelSelectScreen.setLevels(((JsonCircle)actor).getLevels());
                            core.showScreen(levelSelectScreen);
                        }
                    }
                }
            }
        });
    }

    public Actor checkHit(float x, float y, boolean touchable) {
        Actor actor = getChildren().get(safeIndex(getCenter(dx)));
        x -= actor.getX();
        y -= actor.getY();
        return x >= 0 && x < 538 && y >= 0 && y < 538 ? actor : this;
    }

    private void sweepToClosest() {
        float sign = Math.signum(dx);
        float shift = Math.abs(dx % dPos);
        float rightShift = dPos - shift;
        float toDx;
        if (shift < rightShift) {
            toDx = - shift * sign;
        }else{
            toDx = rightShift * sign;
        }
        clearActions();
        addAction(new slideAnim(this, toDx));
    }

    class slideAnim extends Action{

        private final CirclesMetro owner;
        private float addLeft;
        private final float speed = 10;
        private final float sign;

        public slideAnim(CirclesMetro owner, float add)
        {
            this.owner = owner;
            addLeft = Math.abs(add);
            sign = Math.signum(add);
        }

        @Override
        public boolean act(float delta) {
            owner.dx += getAdd(speed * (float)Math.ceil(addLeft) * delta) * sign;
            slideTo(owner.dx);
            return addLeft <= 0;
        }

        private float getAdd(float add) {
            if (addLeft > add)
            {
                addLeft -= add;
                return add;
            }else {
                add = addLeft;
                addLeft = 0;
                return add;
            }
        }
    }

    /*@Override
    public void addActor(Actor actor) {
        slides.add(actor);
    }*/

    private void sweep(float dx, float dy) {
        slideFor(dx);
    }

    public void init()
    {
        ramaX = getWidth() / 2 - rama.getWidth() / 2;
        ramaY = getHeight() - 50 - rama.getHeight();
        titleX = getWidth() / 2;
        titleY = ramaY + 60;
        for (Actor actor : getChildren())
        {
            actor.setVisible(false);
        }
        size = getChildren().size;
        float posY = getHeight() / 2.5f - 269;
        posX0 = - getWidth() * 0.20f - 269;
        posY0 = posY;
        posX1 = getWidth() * 0.15f - 269;
        posY1 = posY;
        posX2 = getWidth() / 2 - 269;
        posY2 = posY;
        posX3 = getWidth() * 0.85f - 269;
        posY3 = posY;
        posX4 = getWidth() * 1.20f - 269;
        posY4 = posY;
        dPos = posX2 - posX1;
        dx = 0;
        slideTo(0);
    }

    private void slideTo(int i)
    {
        slideFor(i * dPos);
    }

    private void slideFor(float pDx)
    {
        dx += pDx;
        slideTo(dx);
    }

    private int getCenter(float pDx)
    {
        if (Math.signum(pDx) > 0) {
            return (int) Math.ceil(-pDx / dPos);
        }else{
            return (int) Math.floor(-pDx / dPos);
        }
    }

    public void slideTo(float pDx) {

        for (Actor actor : getChildren())
        {
            actor.setVisible(false);
        }

        float shift = (pDx % dPos);
        float scaleShift = shift / dPos;
        int i = getCenter(pDx);

        child1 = getChildren().get(safeIndex(i - 2));
        child1.setX(posX0 + shift);
        child1.setY(posY0);
        child1.setScale(0.5f, 0.5f);
        child1.setVisible(true);
        //child.setZIndex(6);
        //super.addActor(child);

        child2 = getChildren().get(safeIndex(i - 1));
        child2.setX(posX1 + shift);
        child2.setY(posY1);
        child2.setScale(0.5f + 0.5f * (scaleShift > 0 ? scaleShift : 0), 0.5f + 0.5f * (scaleShift > 0 ? scaleShift : 0));
        child2.setVisible(true);

        child3 = getChildren().get(safeIndex(i));
        child3.setX(posX2 + shift);
        child3.setY(posY2);
        child3.setScale(1f - 0.5f * Math.abs(scaleShift), 1f - 0.5f * Math.abs(scaleShift));
        child3.setVisible(true);

        child4 = getChildren().get(safeIndex(i + 1));
        child4.setX(posX3 + shift);
        child4.setY(posY3);
        child4.setScale(0.5f + 0.5f * (scaleShift < 0 ? - scaleShift : 0), 0.5f + 0.5f * (scaleShift < 0 ? - scaleShift : 0));
        child4.setVisible(true);

        child5 = getChildren().get(safeIndex(i + 2));
        child5.setX(posX4 + shift);
        child5.setY(posY4);
        child5.setScale(0.5f, 0.5f);
        child5.setVisible(true);

        String name = "";
        if (shift > 0)
        {
            if (shift > 269)
            {
                name = child2.getName();
            }else{
                name = child3.getName();
            }
        }else{
            if (shift < -269)
            {
                name = child4.getName();
            }else{
                name = child3.getName();
            }
        }
        circleNameLabel.setText(name);
        circleNameLabel.setPosition(titleX, titleY);
    }

    private int safeIndex(int i) {
        return (i >= 0) ? (i % size) : (size + ((i + 1) % size) - 1);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(rama, ramaX, ramaY);
        circleNameLabel.draw(batch, parentAlpha);
        super.draw(batch, parentAlpha);
    }
}
