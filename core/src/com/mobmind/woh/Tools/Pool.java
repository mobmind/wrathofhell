package com.mobmind.woh.Tools;

import com.badlogic.gdx.utils.ReflectionPool;
import com.mobmind.woh.Actors.Effects.Lighting;
import com.mobmind.woh.Actors.Effects.UnderLighting;
import com.mobmind.woh.Actors.MobActor;

public class Pool {
	private static Pool instance = new Pool();
	private ReflectionPool<Lighting> lights = new ReflectionPool<Lighting>(Lighting.class);
	private ReflectionPool<UnderLighting> underLights = new ReflectionPool<UnderLighting>(UnderLighting.class);
	private ReflectionPool<MobActor> mobs = new ReflectionPool<MobActor>(MobActor.class);

	// Debug
	private int lightingCount = 0;
	private int underLightingCount = 0;
	private int mobCount = 0;
	public void iterLightingCount(){ lightingCount++; }
	public void iterUnderLightingCount(){ underLightingCount++; }
	public void iterMobCount(){ mobCount++; }
	public int getLightingCount(){ return lightingCount; }
	public int getUnderLightingCount(){ return underLightingCount; }
	public int getMobCount() { return mobCount; }

	// Getters
	public static Pool getInstance(){ return instance; }
	public ReflectionPool<Lighting> getLights() { return lights; }
	public ReflectionPool<UnderLighting> getUnderLights() { return underLights; }
	public ReflectionPool<MobActor> getMobs() { return mobs; }

	private Pool(){}

	public void worker(Lighting l){
		l.freeStateTime();
		lights.free(l);
		l.remove();
	}

	public void worker(UnderLighting uL) {
		uL.freeStateTime();
		underLights.free(uL);
		uL.remove();
	}

	public void worker(MobActor m){
		m.freeStateTime();
		m.freeState();
		mobs.free(m);
		m.remove();
	}
}