package com.mobmind.woh.Tools;

/**
 * Created by seedofpanic on 30.07.2014.
 */
public class AnimData {
    public int width;
    public int height;
    public int frameStart;
    public int frameEnd;
    public float frameTime;
    public float radius;
}
