package com.mobmind.woh.Tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.utils.Json;
import com.mobmind.woh.Json.Serializers.JsonGroupsSerializer;
import com.mobmind.woh.Json.Spawn.JsonGroup;
import com.mobmind.woh.Json.Spawn.JsonLevelData;
import com.mobmind.woh.Json.Spawn.JsonWaveAction;
import com.mobmind.woh.Json.Spawn.JsonWaves;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Screens.GameScreen;

import java.util.Iterator;

/**
 * Created by seedofpanic on 03.08.2014.
 */
public class Level extends Action {

    private final JsonLevelData level;
    private Iterator<JsonWaves> wavesIter;
    private Iterator<JsonWaveAction> groupsIter;
    private JsonWaves currentWave;
    private JsonWaveAction currentAction;
    private boolean done = false;
    private float nextAction;
    private boolean finished;

    public Level(String fileName)
    {
        Json json = new Json();
        json.setSerializer(JsonWaveAction.class, new JsonGroupsSerializer());
        level = json.fromJson(JsonLevelData.class, Gdx.files.internal("Json/Levels/" + fileName + ".json"));
        wavesIter = level.waves.iterator();
        currentWave = wavesIter.next();
        groupsIter = currentWave.actions.iterator();
        currentAction = groupsIter.next();
        currentAction.init();
        nextAction = 0;
    }


    //TODO Необходима проверка исключительных ситуаций
    @Override
    public boolean act(float delta) {
        if (done) {
            return Kernel.getInstance().getGameScreen().checkVictory();
        }
        do {
            if (nextAction == 0) {
                if (!currentAction.isFinished()) {
                    nextAction += currentAction.act();
                } else {
                    if (groupsIter.hasNext()) {
                        currentAction = groupsIter.next();
                        currentAction.init();
                        nextAction += currentAction.time;
                    } else {
                        if (wavesIter.hasNext()) {
                            currentWave = wavesIter.next();
                            groupsIter = currentWave.actions.iterator();
                            currentAction = groupsIter.next();
                            currentAction.init();
                        } else {
                            done = true;
                            finished = true;
                            Kernel.getInstance().getGameScreen().checkVictory();
                            return false;
                        }
                    }
                }
            }
            if (nextAction > delta)
            {
                nextAction -= delta;
                delta = 0;
            }else{
                delta -= nextAction;
                nextAction = 0;
            }
        }while (delta > 0);
        return false;
    }

    public void skipTime()
    {
        nextAction = 0;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
}
