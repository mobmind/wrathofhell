package com.mobmind.woh.Tools;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by seedofpanic on 12.08.2014.
 */
public class RadialFiller {

    public static TextureRegion[] getTextureRegions(int originX, int originY, Pixmap source) {
        TextureRegion[] textureRegions = new TextureRegion[181];
        int w = source.getWidth();
        int h = source.getHeight();
        Pixmap pixmap = new Pixmap(w, h, source.getFormat());
        int hw = originX;
        int hh = originY;
        Color alpha = new Color(0);
        pixmap.setColor(alpha);
        pixmap.fill();
        int curColor = 0;
        pixmap.setColor(Color.WHITE);
        for (int i = 0; i <= 180; i ++) {
            pixmap.setColor(alpha);
            pixmap.fill();
            for (int x = 0; x < w; x++) {
                for (int y = 0; y < h; y++) {
                    curColor = source.getPixel(x, y);
                    float angle = getAlphaSector(x - hw, y - hh);
                    float angleDif = i * 2 - angle;
                    if ((angleDif > 0) && (angleDif < 3)) {
                        if (curColor != 0) {
                            curColor = alphaModColor(curColor, angleDif / 3);
                        }
                    }
                    if (angle < i * 2) {
                        pixmap.drawPixel(x, y, curColor);
                    }else{
                        pixmap.drawPixel(x, y, -256);
                    }
                }
            }
            textureRegions[i] = new TextureRegion(new Texture(pixmap));
        }
        return textureRegions;
    }

    private static int alphaModColor(int curColor, float mod) {
        int alpha = curColor & 255;
        curColor = curColor & (~255);
        int i = Math.round((float)(alpha) * mod);
        return curColor | i;
    }

    private static float getAlphaSector(int x, int y) {
        float angle = (float)Math.atan2(y, x) * MathUtils.radiansToDegrees;
        if (angle < -90){angle += 360;}
        return angle + 90;
    }


}
