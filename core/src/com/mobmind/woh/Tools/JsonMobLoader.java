package com.mobmind.woh.Tools;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AssetLoader;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.mobmind.woh.Json.Mob.JsonMob;
import com.mobmind.woh.Kernel;

/**
 * Created by seedofpanic on 20.08.2014.
 */
public class JsonMobLoader extends AsynchronousAssetLoader<JsonMob, AssetLoaderParameters<JsonMob>> {
    public JsonMobLoader(InternalFileHandleResolver resolver) {
        super(resolver);
    }

    private JsonMob jsonMob;

    @Override
    public void loadAsync(AssetManager manager, String fileName, FileHandle file, AssetLoaderParameters<JsonMob> parameter) {
        jsonMob = Kernel.getInstance().getJson().fromJson(JsonMob.class, file);
    }

    @Override
    public JsonMob loadSync(AssetManager manager, String fileName, FileHandle file, AssetLoaderParameters<JsonMob> parameter) {
        return jsonMob;
    }

    @Override
    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, AssetLoaderParameters<JsonMob> parameter) {
        return null;
    }
}
