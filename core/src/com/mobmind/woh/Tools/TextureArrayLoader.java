package com.mobmind.woh.Tools;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.PixmapLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.utils.Array;

/**
* Created by seedofpanic on 17.08.2014.
*/
class TextureArrayLoader extends AsynchronousAssetLoader<TextureArray, TextureArrayLoader.TextureArrayParameters>
{
    TextureArray textureArray;

    public TextureArrayLoader(FileHandleResolver resolver) {
        super(resolver);
    }

    @Override
    public void loadAsync(AssetManager manager, String fileName, FileHandle file, TextureArrayParameters parameter) {
        textureArray = new TextureArray();
        textureArray.textures = new Array<AssetDescriptor<Texture>>();
        Preloader.loadTextureSequence(fileName, parameter.ext, textureArray.textures, parameter.startWith);
    }

    @Override
    public TextureArray loadSync(AssetManager manager, String fileName, FileHandle file, TextureArrayParameters parameter) {
        return textureArray;
    }

    @Override
    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, TextureArrayParameters parameter) {
        return null;
    }


    static public class TextureArrayParameters extends AssetLoaderParameters<TextureArray> {
        public String ext;
        public int startWith;
        public int frameStart;
        public int frameEnd;
        public float animTime;
    }
}
