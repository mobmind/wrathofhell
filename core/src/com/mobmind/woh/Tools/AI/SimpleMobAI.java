package com.mobmind.woh.Tools.AI;

import com.mobmind.woh.Actors.MobActor;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Screens.GameScreen;
import com.mobmind.woh.Tools.MobState;

/**
 * Created by seedofpanic on 23.08.2014.
 */
public class SimpleMobAI extends MobAI{

    public SimpleMobAI(MobActor owner) {
        super(owner);
    }

    @Override
    public void act(float delta) {
        if (owner.getState() == MobState.Move) {
            float deltaSpeed = owner.getSpeed() * delta;
            float dx = owner.getTargetX() - owner.getX();
            float dy = owner.getTargetY() - owner.getY();
            float distance = (float) Math.hypot(dx, dy);
            if (distance - 100 > deltaSpeed) {
                owner.setPosition(
                        owner.getX() + deltaSpeed * dx / distance,
                        owner.getY() + deltaSpeed * dy / distance
                );
            } else {
                // Пусть останавливается просто, погрешность не значительная, правда при тормозах мобы будут телепортироваться в центр...
                // setPosition(targetX, targetY);
                owner.setState(MobState.Attack);
                owner.setStateTime(0);
            }
        }
    }
}
