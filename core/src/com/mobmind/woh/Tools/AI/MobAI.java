package com.mobmind.woh.Tools.AI;

import com.mobmind.woh.Actors.MobActor;

/**
 * Created by seedofpanic on 23.08.2014.
 */
public abstract class MobAI {
    MobActor owner;
    public abstract void act(float delta);
    MobAI(MobActor owner)
    {
        this.owner = owner;
    }
}
