package com.mobmind.woh.Tools.Skills;

/**
* Created by seedofpanic on 23.08.2014.
*/
public interface skillLevelupable
{
    void setLevel(int level);
    int getLevel();
}
