package com.mobmind.woh.Tools.Skills;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.mobmind.woh.Actors.PlayerActor;
import com.mobmind.woh.Kernel;

/**
 * Created by seedofpanic on 23.08.2014.
 */
public class Skills {
    static PlayerActor player;
    static Kernel core;
    static Array<Skill> skills;

    static public void init(ArrayMap<String, Integer> levels)
    {
        core = Kernel.getInstance();

        skills.clear();
        //Skills initialization
        skills.add(new FireWeaponSkill(levels));

    }

    static public ArrayMap<String, Integer> getSkillsLevels()
    {
        ArrayMap<String, Integer> skillsLevels = new ArrayMap<String, Integer>();

        for (Skill skill : skills)
        {
            if (skill instanceof skillLevelupable)
            {
                skillsLevels.put(skill.getName(), ((skillLevelupable) skill).getLevel());
            }
        }

        return skillsLevels;
    }

}
