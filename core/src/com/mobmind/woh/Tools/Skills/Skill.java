package com.mobmind.woh.Tools.Skills;

/**
* Created by seedofpanic on 23.08.2014.
*/
public abstract class Skill
{
    int level = 0;
    String name;

    abstract void apply();
    String getName()
    {
        return name;
    }
}
