package com.mobmind.woh.Tools.Skills;

import com.badlogic.gdx.utils.ArrayMap;

/**
* Created by seedofpanic on 23.08.2014.
*/
public class FireWeaponSkill extends Skill implements skillLevelupable
{
    String name = "FireWeaponSkill";

    public FireWeaponSkill(ArrayMap<String, Integer> levels) {
        level = levels.get(name);
    }

    @Override
    void apply() {
        switch (level)
        {
            case 0:
                setDamage(5);
                break;
            case 1:
                setDamage(10);
                break;
            case 2:
                setDamage(20);
                break;
            case 3:
                setDamage(40);
        }
    }

    private void setDamage(int damage)
    {
        Skills.player.setFireWeaponDamage(damage);
        Skills.player.setFireWeaponDamageDiff(damage >> 1);
    }


    @Override
    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public int getLevel() {
        return level;
    }
}
