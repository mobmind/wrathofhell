package com.mobmind.woh.Tools;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;
import java.util.Iterator;

public class AnimationDriver {
    private TextureRegion[] tmp;
    private Animation runAnimation;
    private Animation attackAnim;

    public AnimationDriver(Array<AssetDescriptor<Texture>> textures)
    {
        int index = 0;
        tmp = new TextureRegion[textures.size];
        for (AssetDescriptor<Texture> assetTexture : textures)
        {
            Texture texture = Preloader.manager.get(assetTexture);
            tmp[index++] = new TextureRegion(texture, texture.getWidth(), texture.getHeight());
        }
    }

    public AnimationDriver(Texture texture, int frameW, int frameH){
        TextureRegion regions[][] = TextureRegion.split(texture, frameW, frameH);
        tmp = new TextureRegion[texture.getWidth() / frameW * texture.getHeight() / frameH];
        int index = 0;
        for (int i = 0; i < regions.length; i++){
            for (int j = 0; j < regions[i].length; j++){
                tmp[index] = regions[i][j];
                index++;
            }
        }
    }
    public Animation getAnimation(int frameStart, int frameEnd, float animTime){
        Array<Integer> frames = new Array<Integer>();
        frameStart = frameStart - 1;
        for (int i = frameStart; i < frameEnd; i++){
            frames.add(i);
        }
        return getAnimation(frames, animTime);
    }

    public void setRunAnim(int frameStart, int frameEnd, float animTime){
        runAnimation = getAnimation(frameStart, frameEnd, animTime);
    }

    public Animation getAnimation(Array<Integer> frames, float animTime){
        TextureRegion[] finalTmp = new TextureRegion[frames.size];
        int index = 0;
        for (Integer number : frames){
            finalTmp[index++] = tmp[number];
        }
        return new Animation(animTime, finalTmp);
    }

    public Animation getRunAnim()
    {
        return runAnimation;
    }

    public Animation getAttackAnim() {
        return attackAnim;
    }

    public void setAttackAnim(int frameStart, int frameEnd, float animTime) {
        this.attackAnim = getAnimation(frameStart, frameEnd, animTime);
    }
}
