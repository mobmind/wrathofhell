package com.mobmind.woh.Tools;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AssetLoader;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.Array;

/**
 * Created by seedofpanic on 17.08.2014.
 */
public class FreeTypeFontGeneratorLoader extends AsynchronousAssetLoader<FreeTypeFontGenerator, AssetLoaderParameters<FreeTypeFontGenerator>> {

    public FreeTypeFontGeneratorLoader(FileHandleResolver resolver) {
        super(resolver);
    }

    @Override
    public void loadAsync(AssetManager manager, String fileName, FileHandle file, AssetLoaderParameters<FreeTypeFontGenerator> parameter) {

    }

    @Override
    public FreeTypeFontGenerator loadSync(AssetManager manager, String fileName, FileHandle file, AssetLoaderParameters<FreeTypeFontGenerator> parameter) {
        return new FreeTypeFontGenerator(file);
    }

    @Override
    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, AssetLoaderParameters<FreeTypeFontGenerator> parameter) {
        return null;
    }
}
