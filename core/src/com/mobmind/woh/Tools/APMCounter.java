package com.mobmind.woh.Tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

/**
 * Created by seedofpanic on 09.08.2014.
 */
public class APMCounter implements InputProcessor {
    private static long currentAPM = 0;
    private static long lastTouch = 0;
    private static long countingTouchesDone;
    private static long coutingTimeSumm;

    public static long getCurrent() {
        if (countingTouchesDone > 0) {
            return (countingTouchesDone * 60000) / coutingTimeSumm;
        }else{
            return 0;
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        long thisTouch = System.currentTimeMillis();
        if (thisTouch - lastTouch < 1000) {
            countingTouchesDone ++;
            coutingTimeSumm += thisTouch - lastTouch;
        }
        lastTouch = thisTouch;
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
