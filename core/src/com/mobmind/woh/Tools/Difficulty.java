package com.mobmind.woh.Tools;

import com.mobmind.woh.Actors.MobActor;
import com.mobmind.woh.Json.Spawn.JsonGroup;

/**
 * Created by seedofpanic on 21.08.2014.
 */
public class Difficulty {

    private static boolean doubledHelth = false;

    public static boolean isDoubledNumber() {
        return doubledNumber;
    }

    public static void setDoubledNumber(boolean doubledNumber) {
        Difficulty.doubledNumber = doubledNumber;
    }

    public static boolean isDoubledHelth() {
        return doubledHelth;
    }

    public static void setDoubledHelth(boolean doubledHelth) {
        Difficulty.doubledHelth = doubledHelth;
    }

    private static boolean doubledNumber = false;

    public static void modify(MobActor mobActor) {
        if (doubledHelth)
        {
            mobActor.setMaxLife(mobActor.getMaxLife() * 2);
        }
    }

    public static void clear()
    {
        doubledHelth = false;
        doubledNumber = false;
    }

    public static void modify(JsonGroup jsonGroup) {
        if (doubledNumber)
        {
            jsonGroup.count *= 2;
        }
    }
}
