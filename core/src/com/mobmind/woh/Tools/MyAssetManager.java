package com.mobmind.woh.Tools;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;

/**
 * Created by seedofpanic on 17.08.2014.
 */
public class MyAssetManager extends AssetManager{
    @Override
    public synchronized void unload(String fileName) {
        if (isLoaded(fileName)) {
            super.unload(fileName);
        }
    }

    @Override
    public synchronized void load(AssetDescriptor desc) {
        if (!isLoaded(desc.fileName)) {
            super.load(desc);
        }
    }
}
