package com.mobmind.woh.Tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.*;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Logger;
import com.mobmind.woh.Json.Mob.JsonMob;
import com.mobmind.woh.Kernel;

public class Preloader {

    public static AssetDescriptor<Texture> startBtn = new AssetDescriptor<Texture>("Menu/Scroll_UP.png", Texture.class);
    public static boolean menuLoaded;
    public static AssetDescriptor<Texture> menuPreAnimation;
    public static Array<AssetDescriptor<Texture>> menuPreAnims;
    public static AssetDescriptor<Texture> selectBtn = new AssetDescriptor<Texture>("Menu/MENU_start.png", Texture.class);
    public static AssetDescriptor<Texture> newProfile = new AssetDescriptor<Texture>("Menu/MENU_NEW.png", Texture.class);
    public static AssetDescriptor<Texture> deleteBtn = new AssetDescriptor<Texture>("Menu/MENU_delete.png", Texture.class);
    public static AssetDescriptor<Texture> changeBtn = new AssetDescriptor<Texture>("Menu/MENU_change.png", Texture.class);
    public static AssetDescriptor<Texture> header;
    public static AssetDescriptor<Texture> newGameBtn = new AssetDescriptor<Texture>("Menu/MENU_new_game.png", Texture.class);
    public static BitmapFont mainFont22;
    public static BitmapFont mainFont30;
    public static BitmapFont ubuntuFont = new BitmapFont(Gdx.files.internal("Font/ubuntu.fnt"));

	/* Game */
	public static AssetDescriptor<Texture> fortStand = new AssetDescriptor<Texture>("castle/Fort_stand.png", Texture.class);
	public static AssetDescriptor<Texture> fortDestruction = new AssetDescriptor<Texture>("castle/Fort_destruction.png", Texture.class);
	public static AssetDescriptor<Texture> fortUnderAttack = new AssetDescriptor<Texture>("castle/Fort_under_attack.png", Texture.class);
	public static AssetDescriptor<Texture> bull = new AssetDescriptor<Texture>("Mobs/Bull.png", Texture.class);
	public static AssetDescriptor<Texture> fatso = new AssetDescriptor<Texture>("Mobs/Fatso.png", Texture.class);
	public static AssetDescriptor<Texture> golem = new AssetDescriptor<Texture>("Mobs/Golem.png", Texture.class);
	public static AssetDescriptor<Texture> ghost = new AssetDescriptor<Texture>("Mobs/Ghost.png", Texture.class);

    public static AssetDescriptor<Texture> life = new AssetDescriptor<Texture>("castle/life.png", Texture.class);
    public static AssetDescriptor<Texture> lifeHolder = new AssetDescriptor<Texture>("castle/lifeHolder.png", Texture.class);

    public static AssetDescriptor<Texture> cancelBtn = new AssetDescriptor<Texture>("Menu/MENU_cancel.png", Texture.class);
    public static AssetDescriptor<Texture> okBtn = new AssetDescriptor<Texture>("Menu/MENU_ok.png", Texture.class);
    public static AssetDescriptor<Texture> window9p = new AssetDescriptor<Texture>("Menu/window_9p.png", Texture.class);
    public static AssetDescriptor<Texture> pauseDialogBack = new AssetDescriptor<Texture>("Menu/endoflevel/sheet_of_pause.png", Texture.class);
    public static AssetDescriptor<Texture> yesBtn = new AssetDescriptor<Texture>("Menu/MENU_yes.png", Texture.class);
    public static AssetDescriptor<Texture> noBtn = new AssetDescriptor<Texture>("Menu/MENU_no.png", Texture.class);
    public static AssetDescriptor<Texture> levelselectbg = new AssetDescriptor<Texture>("Menu/levelselectbg.jpg", Texture.class);
    public static AssetDescriptor<Texture> skillsbg = new AssetDescriptor<Texture>("Menu/CPkzItIIajo.jpg", Texture.class);
    public static AssetDescriptor<Texture> commingsoon = new AssetDescriptor<Texture>("Menu/comingsoon.png", Texture.class);
    public static AssetDescriptor<Texture> mainMenuBG = new AssetDescriptor<Texture>("Menu/vheexN5xj58.jpg", Texture.class);
    public static AssetDescriptor<Texture> levelbg = new AssetDescriptor<Texture>("level.jpg", Texture.class);
    public static AssetDescriptor<Texture> fogbg = new AssetDescriptor<Texture>("fog.png", Texture.class);
    public static AssetDescriptor<Texture> circle1 = new AssetDescriptor<Texture>("Menu/circle1.png", Texture.class);
    public static AssetDescriptor<Texture> circleClosed = new AssetDescriptor<Texture>("Menu/circle_closed.png", Texture.class);
    public static AssetDescriptor<Texture> ringOfMenu = new AssetDescriptor<Texture>("Menu/Ring_of_menu.png", Texture.class);
    public static AssetDescriptor<Texture> rama = new AssetDescriptor<Texture>("Menu/Rama.png", Texture.class);

    public static AssetDescriptor<Texture> sheetOfLose = new AssetDescriptor<Texture>("Menu/endoflevel/sheet_of_lose.png", Texture.class);
    public static AssetDescriptor<Texture> sheetOfVictory = new AssetDescriptor<Texture>("Menu/endoflevel/sheet_of_victory.png", Texture.class);

    //Icons
    public static AssetDescriptor<Texture> icon_button_menu = new AssetDescriptor<Texture>("Menu/icons/button_menu.png", Texture.class);
    public static AssetDescriptor<Texture> icon_button_pause = new AssetDescriptor<Texture>("Menu/icons/button_pause.png", Texture.class);
    public static AssetDescriptor<Texture> icon_home = new AssetDescriptor<Texture>("Menu/icons/home.png", Texture.class);
    public static AssetDescriptor<Texture> icon_in_menu = new AssetDescriptor<Texture>("Menu/icons/in_menu.png", Texture.class);
    public static AssetDescriptor<Texture> icon_info = new AssetDescriptor<Texture>("Menu/icons/info.png", Texture.class);
    public static AssetDescriptor<Texture> icon_music_off = new AssetDescriptor<Texture>("Menu/icons/music_off.png", Texture.class);
    public static AssetDescriptor<Texture> icon_music_on = new AssetDescriptor<Texture>("Menu/icons/music_on.png", Texture.class);
    public static AssetDescriptor<Texture> icon_play = new AssetDescriptor<Texture>("Menu/icons/play.png", Texture.class);
    public static AssetDescriptor<Texture> icon_play_next = new AssetDescriptor<Texture>("Menu/icons/play_next.png", Texture.class);
    public static AssetDescriptor<Texture> icon_plus;
    public static AssetDescriptor<Texture> icon_restart1 = new AssetDescriptor<Texture>("Menu/icons/restart1.png", Texture.class);
    public static AssetDescriptor<Texture> icon_sounds_off = new AssetDescriptor<Texture>("Menu/icons/sounds_off.png", Texture.class);
    public static AssetDescriptor<Texture> icon_sounds_on = new AssetDescriptor<Texture>("Menu/icons/sounds_on.png", Texture.class);
    public static AssetDescriptor<Texture> icon_stats1 = new AssetDescriptor<Texture>("Menu/icons/stats1.png", Texture.class);
    public static Array<AssetDescriptor<Texture>> sublevels;
    public static Array<AssetDescriptor<Texture>> planks;
    public static AssetDescriptor<Texture> star = new AssetDescriptor<Texture>("Menu/sublevels/Star.png", Texture.class);
    public static Array<AssetDescriptor<Texture>> numbers;
    public static Array<Integer> textKeyFrames;
    public static AssetDescriptor<Texture> icon_fire = new AssetDescriptor<Texture>("Weapons/fire.png", Texture.class);
    public static AssetDescriptor<Texture> icon_cold = new AssetDescriptor<Texture>("Weapons/cold.png", Texture.class);
    public static AssetDescriptor<Texture> icon_lighting = new AssetDescriptor<Texture>("Weapons/lighting.png", Texture.class);
    public static AssetDescriptor<Texture> icon_x = new AssetDescriptor<Texture>("Menu/icons/x.png", Texture.class);
    public static AssetDescriptor<Texture> icon_back = new AssetDescriptor<Texture>("Menu/icons/back.png", Texture.class);
    public static AssetDescriptor<Texture> icon_levelup = new AssetDescriptor<Texture>("Menu/icons/levelup.png", Texture.class);
    public static AssetDescriptor<Texture> icon_forward;

    public static AssetDescriptor<Texture> borderOfButtonMenu = new AssetDescriptor<Texture>("Menu/icons/border_of_button_menu.png", Texture.class);
    public static AssetDescriptor<Texture> borderOfButtonPause = new AssetDescriptor<Texture>("Menu/icons/border_of_button_pause.png", Texture.class);
    public static AssetDescriptor<Texture> dialogBack = new AssetDescriptor<Texture>("Menu/dialog_back.png", Texture.class);
    public static AssetDescriptor<Texture> manaContur = new AssetDescriptor<Texture>("Menu/mana_contur.png", Texture.class);
    public static AssetDescriptor<Texture> mana = new AssetDescriptor<Texture>("Menu/mana.png", Texture.class);
    public static AssetDescriptor<Texture> rageContur = new AssetDescriptor<Texture>("Menu/rage_contur.png", Texture.class);
    public static AssetDescriptor<Texture> rage = new AssetDescriptor<Texture>("Menu/rage.png", Texture.class);

    public static AssetDescriptor<Texture> victoryStar = new AssetDescriptor<Texture>("Menu/endoflevel/victory_star.png", Texture.class);

    public static Array<AssetDescriptor<Texture>> menuHeader;

    public static AssetDescriptor<Texture> loadBar = new AssetDescriptor<Texture>("Menu/loading/Load_bar.png", Texture.class);
    public static AssetDescriptor<Texture> loadLine = new AssetDescriptor<Texture>("Menu/loading/Load_line.png", Texture.class);
    public static AssetDescriptor<Texture> loadSheet = new AssetDescriptor<Texture>("Menu/loading/Load_sheet.png", Texture.class);

    //Particles
    public static AssetDescriptor<ParticleEffect> fireball;
    public static AssetDescriptor<ParticleEffect> frostKickSmash;

    //Lighting effect
    public static TextureRegion[] lightingEffect;
    public static Animation lightingCharge;
    public static Animation lightingKick;
    public static AssetDescriptor<Texture> nails = new AssetDescriptor<Texture>("Menu/sublevels/nails.png", Texture.class);

    //Sounds
    public static AssetDescriptor<Sound> sound_ignite = new AssetDescriptor<Sound>("Sounds/ignite.wav", Sound.class);
    public static AssetDescriptor<Sound> sound_click = new AssetDescriptor<Sound>("Sounds/click.wav", Sound.class);
    public static AssetDescriptor<Sound> sound_scroll_roll = new AssetDescriptor<Sound>("Sounds/scroll_roll.mp3", Sound.class);
    public static AssetDescriptor<Sound> sound_breath = new AssetDescriptor<Sound>("Sounds/breath_faster.wav", Sound.class);
    public static AssetDescriptor<Sound> sound_ice_cracking = new AssetDescriptor<Sound>("Sounds/ice_cracking.wav", Sound.class);
    public static AssetDescriptor<Sound> sound_charge = new AssetDescriptor<Sound>("Sounds/charge.mp3", Sound.class);
    public static AssetDescriptor<Sound> sound_lighting = new AssetDescriptor<Sound>("Sounds/lighting.mp3", Sound.class);
    public static AssetDescriptor<Sound> sound_lighting_start = new AssetDescriptor<Sound>("Sounds/lighting_start.wav", Sound.class);
    public static boolean gameLoaded = false;

    public static MyAssetManager manager = new MyAssetManager();
    private static AssetDescriptor<FreeTypeFontGenerator> generator;
    private static AssetDescriptor<Texture> lightingEffectAsset;
    private static AssetDescriptor<Texture> lightingChargeAsset;
    public static Texture ninePathTexture;
    public static AssetDescriptor<TextureArray> lightingKickAsset;
    public static BitmapFont damageFont22;
    private static AssetDescriptor<FreeTypeFontGenerator> damageFontGenerator;
    public static BitmapFont mainFont18;

    public static void init()
    {
        //manager.setLogger(new Logger("manager", Logger.INFO));
        manager.setLoader(TextureArray.class, new TextureArrayLoader(new InternalFileHandleResolver()));
        manager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(new InternalFileHandleResolver()));
        manager.setLoader(JsonMob.class, new JsonMobLoader(new InternalFileHandleResolver()));

        AssetLoaderParameters<FreeTypeFontGenerator> params = new AssetLoaderParameters<FreeTypeFontGenerator>();
        params.loadedCallback = new AssetLoaderParameters.LoadedCallback() {
            @Override
            public void finishedLoading(AssetManager assetManager, String fileName, Class type) {
                FreeTypeFontGenerator fontGenerator = assetManager.get(fileName, FreeTypeFontGenerator.class);

                FreeTypeFontGenerator.FreeTypeFontParameter params22 = new FreeTypeFontGenerator.FreeTypeFontParameter();
                params22.size = 44;
                mainFont22 = fontGenerator.generateFont(params22);

                FreeTypeFontGenerator.FreeTypeFontParameter params30 = new FreeTypeFontGenerator.FreeTypeFontParameter();
                params30.size = 60;
                mainFont30 = fontGenerator.generateFont(params30);

                FreeTypeFontGenerator.FreeTypeFontParameter params18 = new FreeTypeFontGenerator.FreeTypeFontParameter();
                params18.size = 36;
                mainFont18 = fontGenerator.generateFont(params18);

                fontGenerator.dispose();
            }
        };
        generator = new AssetDescriptor<FreeTypeFontGenerator>("Font/BLKCHCRY.TTF", FreeTypeFontGenerator.class, params);
        //Fonts


        ParticleEffectLoader.ParticleEffectParameter effectsParams = new ParticleEffectLoader.ParticleEffectParameter();
        effectsParams.imagesDir =  Gdx.files.internal("Particles/");
        fireball = new AssetDescriptor<ParticleEffect>("Particles/Fire.prt", ParticleEffect.class, effectsParams);
        frostKickSmash = new AssetDescriptor<ParticleEffect>("Particles/Frost_kick_smash.prt", ParticleEffect.class, effectsParams);

        AssetLoaderParameters<FreeTypeFontGenerator> damageFontParams = new AssetLoaderParameters<FreeTypeFontGenerator>();
        damageFontParams.loadedCallback = new AssetLoaderParameters.LoadedCallback() {
            @Override
            public void finishedLoading(AssetManager assetManager, String fileName, Class type) {
                FreeTypeFontGenerator fontGenerator = assetManager.get(fileName, FreeTypeFontGenerator.class);

                FreeTypeFontGenerator.FreeTypeFontParameter params22 = new FreeTypeFontGenerator.FreeTypeFontParameter();
                params22.size = 44;
                damageFont22 = fontGenerator.generateFont(params22);

                fontGenerator.dispose();
            }
        };

        damageFontGenerator = new AssetDescriptor<FreeTypeFontGenerator>("Font/Domestic_Manners.ttf", FreeTypeFontGenerator.class, damageFontParams);

        textKeyFrames = new Array<Integer>();
        textKeyFrames.add(0);
        textKeyFrames.add(1);
        textKeyFrames.add(2);
        textKeyFrames.add(3);
        textKeyFrames.add(4);
        textKeyFrames.add(5);
        textKeyFrames.add(6);
        textKeyFrames.add(7);
        textKeyFrames.add(8);
        textKeyFrames.add(3);
        textKeyFrames.add(2);
        textKeyFrames.add(1);
        textKeyFrames.add(0);

        preLoadBase();
    }

    /* Getters */
	public static Texture getMobByType(Mob.Type type) {
		if(type == Mob.Type.Bull)
			return manager.get(Preloader.bull);
		if(type == Mob.Type.Fatso)
			return manager.get(Preloader.fatso);
        if (type == Mob.Type.Golem)
            return manager.get(Preloader.golem);
        if (type == Mob.Type.Ghost)
            return manager.get(Preloader.ghost);

		return null;
	}

    //Не выгружаемые ресурсы
    public static void preLoadBase()
    {
        manager.load(generator);
        manager.load(loadBar);
        manager.load(loadLine);
        manager.load(loadSheet);
        manager.finishLoading();
        manager.load(damageFontGenerator);

        manager.load("Menu/dialog_back.png", Texture.class);



        manager.load(window9p);

        //Sounds
        manager.load(sound_ignite);
        manager.load(sound_click);

        //Icons
        manager.load(icon_x);
        manager.load(icon_button_menu);
        manager.load(icon_button_pause);
        manager.load(icon_home);
        manager.load(icon_in_menu);
        manager.load(icon_info);
        manager.load(icon_music_off);
        manager.load(icon_music_on);
        //icon_plus = new Texture(Gdx.files.internal("Menu/icons/plus.png"));
        manager.load(icon_sounds_off);
        manager.load(icon_sounds_on);
        manager.load(icon_stats1);

        manager.load(icon_back);
        //icon_forward = new Texture(Gdx.files.internal("Menu/icons/forward.png"));
        manager.load(skillsbg);
        manager.load(commingsoon);
    }

	public static void preLoadMenu()
    {
        menuHeader = new Array<AssetDescriptor<Texture>>();
        loadTextureSequence("Menu/header/header_", ".png", menuHeader, 1);

        manager.load(startBtn);

        manager.load(newProfile);
        manager.load(selectBtn);
        manager.load(changeBtn);
        manager.load(deleteBtn);
        manager.load(newGameBtn);
        manager.load(okBtn);
        manager.load(cancelBtn);
        manager.load(noBtn);
        manager.load(yesBtn);



        manager.load(mainMenuBG);

        manager.load(icon_levelup);

        menuPreAnims = new Array<AssetDescriptor<Texture>>();
        loadTextureSequence("Menu/Scroll_Roll/Scroll_ROLL_", ".png", menuPreAnims, 1);


        manager.load(sound_scroll_roll);

        menuLoaded = true;

        preloadSelectLevel();
    }

    public static void freeMenu()
    {
        Kernel core = Kernel.getInstance();
        core.getMenuScreen().setBuilt(false);
        core.getCirclesSelectScreen().setBuilt(false);
        core.getLevelSelectScreen().setBuilt(false);

        free(menuHeader);
        manager.unload(startBtn.fileName);
        manager.unload(yesBtn.fileName);
        manager.unload(newProfile.fileName);
        manager.unload(selectBtn.fileName);
        manager.unload(changeBtn.fileName);
        manager.unload(deleteBtn.fileName);
        manager.unload(newGameBtn.fileName);
        manager.unload(okBtn.fileName);
        manager.unload(cancelBtn.fileName);
        manager.unload(noBtn.fileName);
        manager.unload(yesBtn.fileName);

        manager.unload(mainMenuBG.fileName);

        free(menuPreAnims);

        manager.unload(icon_levelup.fileName);

        manager.unload(levelselectbg.fileName);
        manager.unload(circle1.fileName);
        manager.unload(circleClosed.fileName);
        manager.unload(ringOfMenu.fileName);
        manager.unload(rama.fileName);

        free(sublevels);
        
        free(planks);

        manager.unload(star.fileName);
        manager.unload(nails.fileName);
        free(numbers);

        menuLoaded = false;
    }

    private static void free(Array<AssetDescriptor<Texture>> textures) {
        for (AssetDescriptor<Texture> texture : textures)
        {
            manager.unload(texture.fileName);
        }
    }

    //Вызывается вместе с подгрузкой меню, вынесена в отдельный метод на всякий случай
    public static void preloadSelectLevel()
    {
        //Icons

        manager.load(levelselectbg);
        manager.load(circle1);
        manager.load(circleClosed);
        manager.load(ringOfMenu);
        manager.load(rama);

        sublevels = new Array<AssetDescriptor<Texture>>();
        loadTextureSequence("Menu/sublevels/scraps/scrap",".png", sublevels, 1);
        planks = new Array<AssetDescriptor<Texture>>();
        loadTextureSequence("Menu/sublevels/planks/planks_",".png", planks, 1);
        manager.load(star);
        manager.load(nails);
        numbers = new Array<AssetDescriptor<Texture>>();
        loadTextureSequence("Menu/sublevels/numbers/sublayer_number_", ".png", numbers, 1);

    }

    public static void loadTextureSequence(String name, String ext, Array<AssetDescriptor<Texture>> textures, int startWith) {
        int i = startWith;
        while (true)
        {
            FileHandle f = Gdx.files.internal(name + (i++) + ext);
            if (!f.exists()) {break;}
            AssetDescriptor<Texture> asset = new AssetDescriptor<Texture>(f, Texture.class);
            textures.add(asset);
            manager.load(asset);
        }
    }

	public static void preloadGame(){

        //Icons
        manager.load(icon_restart1);
        manager.load(icon_play);
        manager.load(icon_play_next);

        manager.load(levelbg);
        manager.load(fogbg);
        manager.load(fortStand);
        manager.load(fortDestruction);
        manager.load(fortUnderAttack);
        manager.load(bull);
        manager.load(fatso);
        manager.load(golem);
        manager.load(ghost);



        manager.load(fireball);
        manager.load(frostKickSmash);
        manager.load(icon_fire);
        manager.load(icon_cold);
        manager.load(icon_lighting);
        TextureLoader.TextureParameter lightingEffectParams = new TextureLoader.TextureParameter();
        lightingEffectParams.loadedCallback = new AssetLoaderParameters.LoadedCallback() {
            @Override
            public void finishedLoading(AssetManager assetManager, String fileName, Class type) {
                lightingEffect = TextureRegion.split(manager.get(lightingEffectAsset), 204, 205)[0];
            }
        };
        lightingEffectAsset = new AssetDescriptor<Texture>("Weapons/lighting_process.png" , Texture.class, lightingEffectParams);
        manager.load(lightingEffectAsset);

        TextureLoader.TextureParameter lightingChargeParams = new TextureLoader.TextureParameter();
        lightingChargeParams.loadedCallback = new AssetLoaderParameters.LoadedCallback() {
            @Override
            public void finishedLoading(AssetManager assetManager, String fileName, Class type) {
                lightingCharge = new AnimationDriver(manager.get(lightingChargeAsset), 256, 256).getAnimation(1, 4, 0.15f);
            }
        };
        lightingChargeAsset = new AssetDescriptor<Texture>("Weapons/charge.png" , Texture.class, lightingChargeParams);
        manager.load(lightingChargeAsset);


        TextureArrayLoader.TextureArrayParameters lightingKickParams = new TextureArrayLoader.TextureArrayParameters();
        lightingKickParams.ext = ".png";
        lightingKickParams.startWith = 1;
        lightingKickParams.frameStart = 1;
        lightingKickParams.frameEnd = 11;
        lightingKickParams.animTime = 0.15f;

        lightingKickAsset = new AssetDescriptor<TextureArray>("Weapons/lighting_kick/", TextureArray.class, lightingKickParams);
        manager.load(lightingKickAsset);

        manager.load(borderOfButtonMenu);
        manager.load(borderOfButtonPause);
        manager.load(life);
        manager.load(lifeHolder);
        manager.load(pauseDialogBack);
        manager.load(mana);
        manager.load(manaContur);
        manager.load(rage);
        manager.load(rageContur);

        manager.load(sheetOfLose);
        manager.load(sheetOfVictory);
        manager.load(victoryStar);

        //Sounds
        manager.load(sound_breath);
        manager.load(sound_ice_cracking);
        manager.load(sound_charge);
        manager.load(sound_lighting);
        manager.load(sound_lighting_start);


        gameLoaded = true;


	}


    public static void freeGame() {
        Kernel core = Kernel.getInstance();
        core.getGameScreen().setBuilt(false);

        //Icons
        manager.unload(icon_restart1.fileName);
        manager.unload(icon_play.fileName);
        manager.unload(icon_play_next.fileName);

        manager.unload(levelbg.fileName);
        manager.unload(fogbg.fileName);
        manager.unload(fortStand.fileName);
        manager.unload(fortDestruction.fileName);
        manager.unload(fortUnderAttack.fileName);
        manager.unload(bull.fileName);
        manager.unload(fatso.fileName);
        manager.unload(golem.fileName);
        manager.unload(ghost.fileName);
        manager.unload(fireball.fileName);
        manager.unload(frostKickSmash.fileName);
        manager.unload(icon_fire.fileName);
        manager.unload(icon_cold.fileName);
        manager.unload(icon_lighting.fileName);
        free(lightingEffect);
        manager.unload(lightingEffectAsset.fileName);
        free(lightingCharge);
        manager.unload(lightingChargeAsset.fileName);
        free(lightingKick);
        manager.unload(borderOfButtonMenu.fileName);
        manager.unload(borderOfButtonPause.fileName);
        manager.unload(life.fileName);
        manager.unload(lifeHolder.fileName);
        manager.unload(pauseDialogBack.fileName);
        manager.unload(mana.fileName);
        manager.unload(manaContur.fileName);
        manager.unload(rage.fileName);
        manager.unload(rageContur.fileName);
        manager.unload(sheetOfLose.fileName);
        manager.unload(sheetOfVictory.fileName);
        manager.unload(victoryStar.fileName);

        //Sounds
        manager.unload(sound_breath.fileName);
        manager.unload(sound_ice_cracking.fileName);
        manager.unload(sound_charge.fileName);
        manager.unload(sound_lighting.fileName);
        manager.unload(sound_lighting_start.fileName);

        gameLoaded = false;
    }

    private static void free(Animation animation) {
        free(animation.getKeyFrames());
    }

    private static void free(TextureRegion[] textureRegions) {
        for (TextureRegion textureRegion : textureRegions)
        {
            textureRegion.getTexture().dispose();
            textureRegion = null;
        }
    }

    public static void loadMenuData() {
        if (gameLoaded)
        {
            freeGame();
        }
        preLoadMenu();
        gameLoaded = false;
    }

    public static void loadGameData() {
        if (menuLoaded)
        {
            freeMenu();
        }
        preloadGame();
        menuLoaded = false;
    }

    public static void unload() {
        menuLoaded = false;
        gameLoaded = false;
        //manager.clear();
    }
}
