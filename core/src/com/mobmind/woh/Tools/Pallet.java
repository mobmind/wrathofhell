package com.mobmind.woh.Tools;

import com.badlogic.gdx.graphics.Color;

/**
 * Created by seedofpanic on 14.08.2014.
 */
public class Pallet{
    public static Color Brown = new Color(0x2b0501FF);
    public static Color LoadingBackBrown = new Color(0x1c0801FF);
    public static Color Orange = new Color(0xf04517FF);
    public static Color Frizzed = new Color(0x004cffff);
    public static Color LoadingMessage =  new Color(0xf2450fff);
    public static Color LoadingText = new Color(0x1e0d0dff);
}
