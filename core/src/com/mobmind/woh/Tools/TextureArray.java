package com.mobmind.woh.Tools;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Array;

/**
 * Created by seedofpanic on 17.08.2014.
 */
public class TextureArray {
    public Array<AssetDescriptor<Texture>> textures;
}
