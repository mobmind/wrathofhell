package com.mobmind.woh.Tools;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.*;
import com.mobmind.woh.gui.Dialog;

/**
 * Created by seedofpanic on 09.08.2014.
 */
public class DialogBack extends Actor {
    public DialogBack()
    {
        addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        setVisible(false);
    }

    @Override
    protected void setStage(Stage stage) {
        if (stage != null) {
            setWidth(stage.getWidth());
            setHeight(stage.getHeight());
        }
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible)
        {
            toFront();
        }
        super.setVisible(visible);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(Preloader.manager.get(Preloader.dialogBack), getX(), getY(), getWidth(), getHeight());
    }

}
