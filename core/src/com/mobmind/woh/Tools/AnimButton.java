package com.mobmind.woh.Tools;

import com.badlogic.gdx.Audio;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Listeners.Menu.TapListener;

import javax.swing.event.ChangeEvent;


/**
 * Created by seedofpanic on 19.07.2014.
 */
public class AnimButton extends Actor {
    private final Kernel core;
    Animation pressAnim;
    private float stateTime;
    public String logic;
    private TextureRegion currentFrame;
    private boolean pressed;
    private Sound soundEffect;

    private static AnimButton currentPressed;

    public AnimButton(Texture sprite, float x, float y, int width, int height, Array<Integer> keyFrames, float animTime) {
        core = Kernel.getInstance();
        logic = "";
        AnimationDriver animationDriver = new AnimationDriver(sprite, width, height);
        pressAnim = animationDriver.getAnimation(keyFrames, animTime);
        setBounds(x, y, width, height);
        //addListener(new TapListener());
        currentFrame = pressAnim.getKeyFrame(0);
        final AnimButton me = this;

        addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (currentPressed == null)
                {
                    currentPressed = me;
                    pressed = true;
                    if (soundEffect != null)
                    {
                        core.playSound(soundEffect);
                    }
                }
                return true;
            }
        });
    }

    public void setSoundEffect(Sound effect)
    {
        soundEffect = effect;
    }

    @Override
    public void act(float delta) {
        if (pressed) {
            stateTime += delta;
            if (pressAnim.isAnimationFinished(stateTime))
            {
                currentFrame = pressAnim.getKeyFrame(0, false);
                pressed = false;
                currentPressed = null;
                ChangeListener.ChangeEvent event = new ChangeListener.ChangeEvent();
                event.setTarget(this);
                fire(event);
                stateTime = 0;
            }else {
                currentFrame = pressAnim.getKeyFrame(stateTime, true);
            }
        }
        super.act(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(
                currentFrame,
                getX(), getY(),
                getOriginX(), getOriginY(),
                getWidth(), getHeight(),
                getScaleX(), getScaleY(), 0f

        );
    }

}
