package com.mobmind.woh.Tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.mobmind.woh.Kernel;

/**
 * Created by seedofpanic on 09.08.2014.
 */
public class SettingsMenu {

    private final Table prfsWindow;
    private final Button inMenu;
    private final Actor owner;
    private final Kernel core;
    private final ImageButton soundsToggler;
    private final ImageButton musicToggler;

    public SettingsMenu(float buttonX, float buttonY, final Actor owner)
    {
        this.owner = owner;
        final SettingsMenu me = this;
        core = Kernel.getInstance();
        Button.ButtonStyle inMenuStyle = new Button.ButtonStyle();
        inMenuStyle.up = new SpriteDrawable(new Sprite(Preloader.manager.get(Preloader.icon_in_menu)));
        inMenu = new Button(inMenuStyle);
        inMenu.setPosition(buttonX - inMenu.getHeight(), buttonY - inMenu.getHeight());
        inMenu.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.playSound(Preloader.manager.get(Preloader.sound_click));
                soundsToggler.setChecked(!core.getCurrentProfile().soundOn);
                musicToggler.setChecked(!core.getCurrentProfile().musicOn);
                me.setVisible(true);
            }
        });
        Stage stage = core.getStageUI();
        stage.addActor(inMenu);
        prfsWindow = new Table();
        prfsWindow.pad(50);
        prfsWindow.setBackground(new NinePatchDrawable(new NinePatch(Preloader.manager.get(Preloader.window9p), 37, 37, 37, 37)), false);
        stage.addActor(prfsWindow);

        ImageButton.ImageButtonStyle soundsTogglerStyle = new ImageButton.ImageButtonStyle();
        soundsTogglerStyle.imageUp = new SpriteDrawable(new Sprite(Preloader.manager.get(Preloader.icon_sounds_on)));
        soundsTogglerStyle.imageChecked = new SpriteDrawable(new Sprite(Preloader.manager.get(Preloader.icon_sounds_off)));
        soundsToggler = new ImageButton(soundsTogglerStyle);
        soundsToggler.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.setSoundsOn(!soundsToggler.isChecked());
                core.playSound(Preloader.manager.get(Preloader.sound_click));
            }
        });

        ImageButton.ImageButtonStyle musicTogglerStyle = new ImageButton.ImageButtonStyle();
        musicTogglerStyle.imageUp = new SpriteDrawable(new Sprite(Preloader.manager.get(Preloader.icon_music_on)));
        musicTogglerStyle.imageChecked = new SpriteDrawable(new Sprite(Preloader.manager.get(Preloader.icon_music_off)));
        musicToggler = new ImageButton(musicTogglerStyle);
        musicToggler.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.setMusicOn(!soundsToggler.isChecked());
                core.playSound(Preloader.manager.get(Preloader.sound_click));
            }
        });

        ImageButton.ImageButtonStyle menuCloserStyle = new ImageButton.ImageButtonStyle();
        menuCloserStyle.imageUp = new SpriteDrawable(new Sprite(Preloader.manager.get(Preloader.icon_x)));
        ImageButton menuCloser = new ImageButton(menuCloserStyle);
        menuCloser.setPosition(700, 300);
        prfsWindow.addActor(menuCloser);

        menuCloser.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                me.setVisible(false);
                core.playSound(Preloader.manager.get(Preloader.sound_click));
                core.saveCurrentProfile();
                if (owner != null)
                {
                    owner.setVisible(true);
                }
                return true;
            }
        });

        prfsWindow.add(soundsToggler).padRight(50);
        prfsWindow.add(musicToggler).padRight(50);
        prfsWindow.add().padLeft(25);
        prfsWindow.setVisible(false);
        prfsWindow.setWidth(700);
        prfsWindow.setHeight(300);

        prfsWindow.setPosition(stage.getWidth() / 2 - prfsWindow.getWidth() / 2, stage.getHeight() / 2 - prfsWindow.getHeight() / 2);
    }

    public void setVisible(boolean visible)
    {
        if (owner != null)
        {
            owner.setVisible(true);
        }
        Kernel.getInstance().getDialogBack().setVisible(visible);
        prfsWindow.toFront();
        prfsWindow.setVisible(visible);

    }

    public Button getButton()
    {
        return inMenu;
    }
}
