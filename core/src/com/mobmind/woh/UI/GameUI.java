package com.mobmind.woh.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.mobmind.woh.Kernel;

public class GameUI {

	public GameUI(){
		Kernel core = Kernel.getInstance();
		Stage stageUI = core.getStageUI();
		TextureAtlas atlas = new TextureAtlas(Gdx.files.classpath("assets/Atlas/UI/UI.pack"));
		Skin skin = new Skin(atlas);

		/* Viewport по размеру экрана а не по заданному как в игре */
//		int widthPanel = (core.getViewportWidth() - core.getViewportHeight()) / 2;
		int widthPanel = (Gdx.graphics.getWidth() - Gdx.graphics.getHeight()) / 2;
		Gdx.app.debug("widthPanel", Integer.toString(widthPanel));

		Table table = new Table();
		table.setFillParent(true);
		stageUI.addActor(table);

		table.debug();

		Table rightPanel = new Table();
		Table leftPanel = new Table();
		leftPanel.setBackground(skin.getDrawable("board"));
		rightPanel.setBackground(skin.getDrawable("board"));
		table.add(leftPanel).left().width(widthPanel).fill();
		table.add(new Table()).expand();
		table.add(rightPanel).right().width(widthPanel).fill();
	}
}
