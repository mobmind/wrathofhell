package com.mobmind.woh.Templates;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Stage;

public interface TemplateScreen extends Screen{

	Stage getStage();
	Stage getStageUI();

	@Override
	void render(float delta);

	@Override
	void resize(int width, int height);

	@Override
	void show();

	@Override
	void hide();

	@Override
	void pause();

	@Override
	void resume();

	@Override
	void dispose();
}
