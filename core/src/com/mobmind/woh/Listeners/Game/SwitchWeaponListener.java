package com.mobmind.woh.Listeners.Game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.mobmind.woh.Actors.PlayerActor;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Tools.Weapon;

public class SwitchWeaponListener extends InputListener{

	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		PlayerActor player = Kernel.getInstance().getPlayer();
		Gdx.app.debug("Keycode", Integer.toString(keycode));

		switch (keycode){
			case 8 : // 1
				player.setWeapon(Weapon.Type.Fire);
			break;

			case 9 : // 2
				player.setWeapon(Weapon.Type.Frost);
			break;

			case 10 : // 3
                player.setWeapon(Weapon.Type.Light);
			break;
		}
		Gdx.app.debug("Weapon", player.getWeapon().toString());
		return true;
	}
}
