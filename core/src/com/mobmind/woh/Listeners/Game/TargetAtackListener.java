package com.mobmind.woh.Listeners.Game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g3d.utils.ShaderProvider;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.utils.Array;
import com.mobmind.woh.Actors.Effects.LightingActor;
import com.mobmind.woh.Actors.MobActor;
import com.mobmind.woh.Actors.ParticleEffectActor;
import com.mobmind.woh.Actors.PlayerActor;
import com.mobmind.woh.DamageText;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Screens.GameScreen;
import com.mobmind.woh.Tools.MobState;
import com.mobmind.woh.Tools.Preloader;
import com.mobmind.woh.Tools.Weapon;

import java.util.Vector;


public class TargetAtackListener extends DragListener {

	private final Kernel core;
	private PlayerActor player;
    private GameScreen gameScreen;
    private boolean isTouched;
    private Array<MobActor> markedMobs;
    private Array<MobActor> affectedMobs;

    public TargetAtackListener(GameScreen gameScreen) {
		core = Kernel.getInstance();
		player = Kernel.getInstance().getPlayer();
        this.gameScreen = gameScreen;
        //isTouched = false;
        markedMobs = new Array<MobActor>(3);
        affectedMobs = new Array<MobActor>();
	}

	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        //if (isTouched) {return false;} else {isTouched = true;}
        if (Kernel.paused) {return false;}
		Weapon.Type type = player.getWeapon();
		int damage;
        //gameScreen.circlex = x;
        //gameScreen.circley = y;

        switch (type){
            case Fire:
                core.playSound(Preloader.manager.get(Preloader.sound_breath));
                ParticleEffect pe = new ParticleEffect(Preloader.manager.get(Preloader.fireball));
                ParticleEffectActor effect = new ParticleEffectActor(pe);
                effect.setPosition(x, y);
                float pScale = player.getFireWeaponRange() / 50;

                float scaling = pe.getEmitters().get(0).getScale().getHighMax();
                pe.getEmitters().get(0).getScale().setHigh(scaling * pScale);

                scaling = pe.getEmitters().get(0).getScale().getLowMax();
                pe.getEmitters().get(0).getScale().setLow(scaling * pScale);

                scaling = pe.getEmitters().get(0).getVelocity().getHighMax();
                pe.getEmitters().get(0).getVelocity().setHigh(scaling * pScale);

                scaling = pe.getEmitters().get(0).getVelocity().getLowMax();
                pe.getEmitters().get(0).getVelocity().setLow(scaling * pScale);

                Kernel.getInstance().getStage().addActor(effect);
                int mobsHited = 0;
                int mobsScore = 0;
	            for(Actor a : gameScreen.getMobsGroup().getChildren()){
		            MobActor mob = ((MobActor) a).hit(x, y);
		            if(mob != null){
			            damage = player.getFireWeaponDamage();
			            attack(mob, damage);
                        mobsHited++;
                        switch (mob.getState())
                        {
                            case Move:
                                mobsScore += 5 * damage;
                                break;
                            case Free:
                                if (mob.getFrizzedState() == MobState.Move) {
                                    mobsScore += 6 * damage;
                                }else{
                                    mobsScore += 2 * damage;
                                }
                                break;
                            case Attack:
                                mobsScore += damage;
                                break;
                        }
		            }
	            }
                core.getGameScreen().addScore(mobsScore * mobsHited);
            break;

            case Light:
                for(Actor a : gameScreen.getMobsGroup().getChildren()){
                    MobActor mob = ((MobActor) a).hit(x, y);
                    if(mob != null){
                        if (player.lightingIsCharged()) {
                            lightingAttack(mob);
                        }
                        break;
                    }
                }
                return false;
        }
		return true;
	}

    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        if (player.weapon == Weapon.Type.Frost && player.mana > 0){
            float delta = Gdx.graphics.getDeltaTime();
            if (Gdx.input.isTouched())
            {
                if (core.getStageUI().hit(x, y, true) == null) {
                    float manaUse = player.frizzManaUse * delta;
                    float frizzPower;
                    if (player.mana > manaUse)
                    {
                        player.mana -= manaUse;
                        frizzPower = player.getFrizzTime() * delta;
                    }else{
                        frizzPower = player.getFrizzTime() * delta * (player.mana / manaUse);
                        player.mana = 0;
                        player.setWeapon(Weapon.Type.Fire);
                    }
                    player.frostKickSmashEffect.effect.setPosition(x, y);
                    player.manaUsed = true;


                    for (Actor a : core.getGameScreen().getMobsGroup().getChildren()) {
                        MobActor mob = ((MobActor) a).hit(x, y);
                        if (mob != null) {
                            mob.frizz(frizzPower);
                        }
                    }
                }
            }
        }
        super.touchDragged(event, x, y, pointer);
    }

    public void lightingAttack(MobActor mob)
    {
        mob.setMarked(true);
        markedMobs.add(mob);
        if (markedMobs.size == 3)
        {
            player.rechargeLighting();
            lightingDoDamage();
            player.setWeapon(Weapon.Type.Fire);
        }
    }

    private void lightingDoDamage() {
        float damage = player.getLightingWeaponDamage();
        Stage stage = Kernel.getInstance().getStage();
        stage.addActor(new LightingActor(markedMobs, getAffectedMobs()));
        for (MobActor mob : markedMobs)
        {
            mob.addDamage(damage);
            mob.setMarked(false);
        }

        int totalDamage = 0;
        for (MobActor mob : affectedMobs)
        {
            mob.addDamage(damage * 3);
            totalDamage += Math.floor(damage * 3);
        }

        core.getGameScreen().addScore(affectedMobs.size * totalDamage);

        markedMobs.clear();
        affectedMobs.clear();
    }

    private Array<MobActor> getAffectedMobs() {
        affectedMobs.clear();
        //TODO рассчитывать это все во время отметки монстров
        float x1 = markedMobs.get(0).getX();
        float y1 = markedMobs.get(0).getY();
        float x2 = markedMobs.get(1).getX();
        float y2 = markedMobs.get(1).getY();
        float x3 = markedMobs.get(2).getX();
        float y3 = markedMobs.get(2).getY();

        /*float angle2 = new Vector2(x2 - x1, y2 - y1).angle();
        float angle3 = new Vector2(x3 - x1, y3 - y1).angle();
        if (angle2 > angle3)
        {
            if (angle2 - angle3 > 180)
            {

            }else {
                x2 = markedMobs.get(2).getX();
                y2 = markedMobs.get(2).getY();
                x3 = markedMobs.get(1).getX();
                y3 = markedMobs.get(1).getY();
            }
        }else{
            if (angle3 - angle2 > 180)
            {
                x2 = markedMobs.get(2).getX();
                y2 = markedMobs.get(2).getY();
                x3 = markedMobs.get(1).getX();
                y3 = markedMobs.get(1).getY();
            }else {

            }
        }*/


        float k1 = (y2 - y1) / (x2 - x1);
        float k2 = (y3 - y2) / (x3 - x2);
        float k3 = (y1 - y3) / (x1 - x3);

        float b1 = y1 - k1 * x1;
        float b2 = y2 - k2 * x2;
        float b3 = y3 - k3 * x3;

        boolean lefted2 = k2 * x1 + b2 > y1;
        boolean lefted3 = k3 * x2 + b3 > y2;
        boolean lefted1 = k1 * x3 + b1 > y3;

        /*if (k1 * x3 + b1 > y3)
        {
            x2 = markedMobs.get(2).getX();
            y2 = markedMobs.get(2).getY();
            x3 = markedMobs.get(1).getX();
            y3 = markedMobs.get(1).getY();


            k1 = (y2 - y1) / (x2 - x1);
            k2 = (y3 - y2) / (x3 - x2);
            k3 = (y1 - y3) / (x1 - x3);

            b1 = y1 - k1 * x1;
            b2 = y2 - k2 * x2;
            b3 = y3 - k3 * x3;
        }*/


        float t1;
        float t2;
        float t3;

        for(Actor a : gameScreen.getMobsGroup().getChildren()){
            MobActor mob = ((MobActor) a);
            if (markedMobs.contains(mob, true)) {continue;}
            t1 = k1 * mob.getX() + b1;
            t2 = k2 * mob.getX() + b2;
            t3 = k3 * mob.getX() + b3;
            float y = mob.getY();
            if(

                    (lefted1 ? (y < t1) : (y > t1)) &&//>
                            (lefted2 ? (y < t2) : (y > t2)) &&//<
                            (lefted3 ? (y < t3) : (y > t3))    //>

            )
            {
                affectedMobs.add(mob);
            }
        }
        return affectedMobs;
    }

    public void attack(MobActor mob, int damage){
        mob.addDamage(damage);
	}

    public void setIsTouched(boolean isTouched) {
        this.isTouched = isTouched;
    }
}
