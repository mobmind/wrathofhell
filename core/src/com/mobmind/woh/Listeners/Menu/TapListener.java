package com.mobmind.woh.Listeners.Menu;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Screens.MenuScreen;
import com.mobmind.woh.Tools.AnimButton;
import com.mobmind.woh.gui.InputDialog;

public class TapListener extends InputListener {

    Kernel core;

    public TapListener()
    {
        core = Kernel.getInstance();
    }

    private boolean flag = true;

    @Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        Actor actor = event.getTarget();
        if (actor != null) {
            if (actor.getClass() == AnimButton.class) {

            } else if (flag) {
                flag = false;
                core.getMenuScreen().initMenu();
            }
        }
		return false;
	}
}
