package com.mobmind.woh;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.ReflectionPool;
import com.mobmind.woh.Actors.Effects.Lighting;
import com.mobmind.woh.Actors.Effects.UnderLighting;
import com.mobmind.woh.Actors.PlayerActor;
import com.mobmind.woh.Json.User.JsonProfile;
import com.mobmind.woh.Json.User.JsonProfiles;
import com.mobmind.woh.Screens.*;
import com.mobmind.woh.Templates.TemplateScreen;
import com.mobmind.woh.Tools.APMCounter;
import com.mobmind.woh.Tools.DialogBack;
import com.mobmind.woh.Tools.Preloader;
import com.mobmind.woh.Tools.Skills.Skills;
import javafx.scene.control.TreeItem;

import java.util.Random;
import java.util.ResourceBundle;

public class Kernel extends Game {

	public final int logLvl = Application.LOG_DEBUG;
//	public final int logLvl = Application.LOG_NONE;
	public static boolean paused = false;

	private final static int viewportWidth = 1280;
	private final static int viewportHeight = 800;
	private GameScreen game;
	private MenuScreen menu;
	private TemplateScreen level;
    private CirclesSelectScreen circlesSelectScreen;
    private LevelSelectScreen levelSelectScreen;
    private TemplateScreen curScreen;
    private static Kernel instance = new Kernel();
	private BitmapFont font;
	private Batch batch;
	private Skin skinDamageText;
    private Json json = new Json();
    private JsonProfiles profiles;
    private JsonProfile currentProfile;
    private Preferences saves;
    private String currentLevel;
    private EndGameScreen endGameScreen;
    private final Random random = new Random();
	private DialogBack dialogBack;
    private static String version = "alpha";
    private LoadingScreen loadingScreen;
    private boolean loaded = false;
    private boolean systemPause;
    private boolean created = false;
    private SkillsScreen skillsScreen;

    /* Singelton  constructor (K.O.) */
    private Kernel(){}

	/* getters */
    public static Kernel getInstance(){ return instance; }
	public TemplateScreen getCurScreen(){ return curScreen; }
	public GameScreen getGameScreen(){ return game; }
	public MenuScreen getMenuScreen(){ return menu; }
    public LevelSelectScreen getLevelSelectScreen(){ return levelSelectScreen;}
	public Stage getStage(){ return getCurScreen().getStage(); }
	public Stage getStageUI(){ return getCurScreen().getStageUI(); }
	public PlayerActor getPlayer(){ return ((GameScreen) getCurScreen()).getPlayer(); }
	public static int getViewportHeight(){ return viewportHeight; }
	public static int getViewportWidth(){ return viewportWidth; }
	public float getRandom(){ return random.nextFloat(); }
	public Skin getSkinDamageText() { return skinDamageText; }
    public Json getJson() { return json; }
    public JsonProfile getCurrentProfile(){return currentProfile;}
    public DialogBack getDialogBack() {return dialogBack;}
	public CirclesSelectScreen getCirclesSelectScreen() { return circlesSelectScreen; }
	public String getCurrentLevel() { return currentLevel; }
	public EndGameScreen getEndGameScreen() { return endGameScreen; }
	public int getRandomInt(int bound) { return random.nextInt(bound); }

	/* setters */
	public void setCurrentLevel(String currentLevel) { this.currentLevel = currentLevel; }
	public void setEndGameScreen(EndGameScreen endGameScreen) { this.endGameScreen = endGameScreen; }
	public void setCurScreen(TemplateScreen screen) { curScreen = screen; }
	public void showScreen(TemplateScreen screen){
		setCurScreen(screen);
		setScreen(screen);
	}

	@Override
	public void create() {
        if (created) {
            //Preloader.preLoadBase();
        }else{
            Preloader.init();
            dialogBack = new DialogBack();

            game = new GameScreen();
            menu = new MenuScreen();
            circlesSelectScreen = new CirclesSelectScreen();
            levelSelectScreen = new LevelSelectScreen();
            skillsScreen = new SkillsScreen();
            loadingScreen = new LoadingScreen();
            Gdx.app.setLogLevel(logLvl);

            showScreen(menu);


            font = new BitmapFont(Gdx.files.internal("Font/ubuntu.fnt"));
            skinDamageText = new Skin(Gdx.files.internal("Json/DamageText.json"));

            batch = getStage().getBatch();
            created = true;
        }
	}

    public void loadGame()
    {
        if (loaded) {return;}
        saves = Gdx.app.getPreferences("woh.saves");
        profiles = json.fromJson(JsonProfiles.class, saves.getString("profiles"));
        if (profiles == null)
        {
            profiles = new JsonProfiles();
        }
        if ((profiles.getProfiles() == null) || (profiles.getProfiles().size == 0))
        {
            getMenuScreen().askNewProfile();
        }else {
            Preferences profileSaves = Gdx.app.getPreferences(profiles.getCurrent().name + ".profile");
            currentProfile = json.fromJson(JsonProfile.class, profileSaves.getString("profile"));
            getMenuScreen().updateCurProfile();
        }
        loaded = true;
    }

	@Override
	public void render() {
        if (systemPause) {return;}
		super.render();

		if(logLvl == Application.LOG_DEBUG){
			batch.begin();
				font.draw(batch, "FPS: " + Gdx.graphics.getFramesPerSecond(), 20, 30);
            font.draw(batch, "V: " + version, 1145, 30);
			batch.end();
		}
	}

	@Override
	public void dispose() {
        super.dispose();
    }

	@Override
	public void resize(int w, int h) { super.resize(w, h); }

	@Override
	public void pause() {
        Gdx.graphics.setContinuousRendering(false);
        systemPause = true;
        super.pause();
    }

	@Override
	public void resume() {
        Gdx.graphics.setContinuousRendering(true);
        systemPause = false;
        super.resume();
    }

    public Array<JsonProfile> getProfilesList() {
        return profiles.getProfiles();
    }

    public void setCurProfile(String name) {
        try {
            profiles.setCurProfile(name);
            Preferences profileSaves = Gdx.app.getPreferences(name + ".profile");
            currentProfile = json.fromJson(JsonProfile.class, profileSaves.getString("profile"));
            if (currentProfile == null)
            {
                currentProfile = new JsonProfile();
                currentProfile.name = name;
                saveCurrentProfile();
                profileSaves = Gdx.app.getPreferences(name + ".profile");
                currentProfile = json.fromJson(JsonProfile.class, profileSaves.getString("profile"));
            }
            Skills.init(currentProfile.skillsLevels);
            saveProfiles();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void deletePrfile() {
        Preferences profilePrefs = Gdx.app.getPreferences(currentProfile.name + ".profile");
        profilePrefs.clear();
        profilePrefs.flush();
        profiles.deleteProfile(currentProfile);
        saveProfiles();
        if (profiles.getProfiles().size > 0) {
            setCurProfile(profiles.getProfiles().first().name);
            getMenuScreen().updateCurProfile();
        }else{
            getMenuScreen().askNewProfile();
        }
    }

    public void newProfile(String name) {
        currentProfile = profiles.addProfile(name);
        saveProfiles();
        saveCurrentProfile();
        getMenuScreen().updateCurProfile();
    }

    public void saveProfiles()
    {
        saves.putString("profiles", json.toJson(profiles));
        saves.flush();
    }

    public void saveCurrentProfile()
    {
        Preferences profileSaves = Gdx.app.getPreferences(currentProfile.name + ".profile");
        profileSaves.putString("profile", json.toJson(currentProfile));
        profileSaves.flush();
    }

    public boolean validateProfileName(String name) {
        return !profiles.profileExists(name);
    }

    public void renameProfile(String newName) {
        String oldProfile = currentProfile.name;
        if (oldProfile.equals(newName)) {return;}
        currentProfile.name = newName;

        profiles.getCurrent().name = newName;
        try {
            profiles.setCurProfile(newName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        saveProfiles();
        saveCurrentProfile();
        getMenuScreen().updateCurProfile();
        Preferences profilePrefs = Gdx.app.getPreferences(oldProfile + ".profile");
        profilePrefs.clear();
        profilePrefs.flush();

    }


    public long playSound(Sound sound) {
        if ((currentProfile != null) && currentProfile.soundOn)
        {
            return sound.play();
        }
        return 0;
    }

    public void playMusic(Music music) {
        if (currentProfile.soundOn)
        {
            music.play();
        }
    }

    public void stopMusic(Music music)
    {
        music.stop();
    }

    public void setSoundsOn(boolean checked) {
        currentProfile.soundOn = checked;
    }

    public void setMusicOn(boolean checked) {
        currentProfile.musicOn = checked;
    }

    public long loopSound(Sound sound) {
        if (currentProfile.soundOn)
        {
            return sound.loop();
        }
        return 0;
    }

    public LoadingScreen getLoadingScreen() {
        return loadingScreen;
    }

    public void stopSound(Sound sound, long soundId) {
        sound.stop(soundId);
    }

    public SkillsScreen getSkillsScreen() {
        return skillsScreen;
    }
}
