package com.mobmind.woh.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mobmind.woh.Actors.Menu.WeaponSwitcher;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Screens.GameScreen;
import com.mobmind.woh.Tools.AnimationDriver;
import com.mobmind.woh.Tools.Preloader;
import com.mobmind.woh.Tools.Weapon;
import jdk.nashorn.internal.codegen.Emitter;

public class PlayerActor extends Actor {

    private final Kernel core;
    private final Animation destructionAnim;
    private final Animation underAttackAnim;
    private final Viewport viewport;
    private final OrthographicCamera camera;
    public Stage stage;
	private Texture sprite;
    public Weapon.Type weapon = Weapon.Type.Fire;
    private float fireWeaponRange;
    private int fireWeaponDamage = 5;
    private int fireWeaponDamageDiff = 5;
    private Animation anim;
    private float stateTime;
    private float lightingWeaponRange;
    private float hp;
    private int underAttak;
    private TextureRegion currentFrame;
    private float maxHp;
    private float lightingWeaponDamage;
    private float frizzTime = 50;
    private float frostWeaponRange = 50;
    private boolean fail = false;
    private boolean stoped;
    public float mana;
    private float maxMana = 50;
    private float manaRegen = 1;
    public float frizzManaUse = 25;
    private float lightingCD = 10;
    private float lightingCDCharge = 10;
    private float lightingChargeSpeed = 1;
    public ParticleEffectActor frostKickSmashEffect;
    private boolean frizzIsVisible;
    private float frizzStateTime;
    private Array<ParticleEmitter> frostEmittersBuffer;
    private long freezeSoundId;
    public boolean manaUsed = false;


    public Weapon.Type getWeapon() { return weapon; }

    public void setWeapon(Weapon.Type weaponType) {
        this.weapon = weaponType;
        switch (weaponType) {
            case Fire:
                Kernel.getInstance().getGameScreen().setWeaponRange(getFireWeaponRange());
                break;
            case Frost:
                Kernel.getInstance().getGameScreen().setWeaponRange(getFrostWeaponRange());
                break;
            case Light:
                Kernel.getInstance().getGameScreen().setWeaponRange(getLightingWeaponRange());
                break;
        }
    }

	public PlayerActor(){
		core = Kernel.getInstance();

        viewport = core.getStage().getViewport();
        camera = new OrthographicCamera();

        fireWeaponDamage = 5;
        fireWeaponRange = 40;
        lightingWeaponRange = 0;
        lightingWeaponDamage = 20;
		sprite = Preloader.manager.get(Preloader.fortStand);

        AnimationDriver animationDriver = new AnimationDriver(sprite, 182, 130);
        anim = animationDriver.getAnimation(1, 5, 0.2f);

        AnimationDriver destructionDriver = new AnimationDriver(Preloader.manager.get(Preloader.fortDestruction), 182,130);
        destructionAnim = destructionDriver.getAnimation(1, 5, 0.2f);

        AnimationDriver underAttackDriver = new AnimationDriver(Preloader.manager.get(Preloader.fortUnderAttack), 182,130);
        underAttackAnim = underAttackDriver.getAnimation(1, 8, 0.2f);

		stage = Kernel.getInstance().getStage();
		setSize(182, 130);
		setOrigin(91, 65);
		setPosition(
			(core.getViewportWidth() / 2) - getOriginX(),
			(core.getViewportHeight() / 2)-getOriginY()
		);

		setName("player");

        hp = 1000;
        maxHp = 1000;

        Kernel.getInstance().getGameScreen().getStageUI().addActor(new UI(this));
        currentFrame = anim.getKeyFrame(0);

        prepareEffects();
	}

    private void prepareEffects() {
        ParticleEffect pe = new ParticleEffect(Preloader.manager.get(Preloader.frostKickSmash));
        frostKickSmashEffect = new ParticleEffectActor(pe);
        float pScale = getFrostWeaponRange() / 50;

        float scaling = pe.getEmitters().get(0).getScale().getHighMax();
        pe.getEmitters().get(0).getScale().setHigh(scaling * pScale);

        scaling = pe.getEmitters().get(0).getScale().getLowMax();
        pe.getEmitters().get(0).getScale().setLow(scaling * pScale);

        scaling = pe.getEmitters().get(0).getVelocity().getHighMax();
        pe.getEmitters().get(0).getVelocity().setHigh(scaling * pScale);

        scaling = pe.getEmitters().get(0).getVelocity().getLowMax();
        pe.getEmitters().get(0).getVelocity().setLow(scaling * pScale);

        for (ParticleEmitter emitter : frostKickSmashEffect.effect.getEmitters())
        {
            emitter.setContinuous(false);
        }

        frostKickSmashEffect.effect.update(10000);
    }

    @Override
    public void act(float delta) {
        if (stoped) {return;}
        stateTime += delta;
        if (fail) {
            if (destructionAnim.isAnimationFinished(stateTime))
            {
                GameScreen gameScreen = Kernel.getInstance().getGameScreen();
                gameScreen.setWin(false);
                gameScreen.showPostGameStats();
                stoped = true;
            }
            currentFrame = destructionAnim.getKeyFrame(stateTime, false);
        }else{
            if (Kernel.paused) {return;}


            frostKickSmashEffect.effect.update(delta);

            if (manaUsed)
            {
                if (!frizzIsVisible) {
                    if (freezeSoundId == 0)
                    {
                        freezeSoundId = core.loopSound(Preloader.manager.get(Preloader.sound_ice_cracking));
                    }
                    frizzIsVisible = true;
                    for (ParticleEmitter emitter : frostKickSmashEffect.effect.getEmitters())
                    {
                        emitter.setContinuous(true);
                    }
                }
            }else
            {
                if (mana < maxMana) {
                    mana += delta * manaRegen;
                    if (mana > maxMana) {
                        mana = maxMana;
                    }
                }
                if (frizzIsVisible)
                {
                    for (ParticleEmitter emitter : frostKickSmashEffect.effect.getEmitters())
                    {
                        emitter.setContinuous(false);
                    }
                    frizzIsVisible = false;
                    if (freezeSoundId > 0)
                    {
                        Preloader.manager.get(Preloader.sound_ice_cracking).stop(freezeSoundId);
                        freezeSoundId = 0;
                    }
                }
            }
            manaUsed = false;
            if (lightingCDCharge < lightingCD) {
                lightingCDCharge += delta * lightingChargeSpeed;
                if (lightingCDCharge > lightingCD)
                {
                    lightingCDCharge = lightingCD;
                }
            }

            if (underAttak > 0) {
                if (underAttackAnim.isAnimationFinished(stateTime)) {
                    stateTime = 0;
                    underAttak--;
                }
                currentFrame = underAttackAnim.getKeyFrame(stateTime, false);
            } else {
                currentFrame = anim.getKeyFrame(stateTime, true);
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
	    batch.draw(
		    currentFrame,
		    getX(), getY(),
		    getOriginX(), getOriginY(),
		    getWidth(), getHeight(),
		    getScaleX(), getScaleY(),
		    getRotation()
        );
    }

    public float getFireWeaponRange() {
        return fireWeaponRange;
    }

    public int getFireWeaponDamage() {
        return fireWeaponDamage + core.getRandomInt(fireWeaponDamageDiff);
    }

    public float getLightingWeaponDamage() {
        return lightingWeaponDamage;
    }

    public float getLightingWeaponRange() {
        return lightingWeaponRange;
    }

    public void setLightingWeaponRange(float lightingWeaponRange) {
        this.lightingWeaponRange = lightingWeaponRange;
    }

    public void addDamage(float damage) {
        hp -= damage;
        if (hp <= 0)
        {
            stateTime = 0;
            core.getGameScreen().gameOver();
            hp = 0;
            fail = true;
        }else{
            if (underAttak == 0)
            {
                stateTime = 0;
            }
            if (underAttak < 2) {
                underAttak++;
            }
        }
    }

    public float getFrizzTime() {
        return frizzTime;
    }

    public void setFrizzTime(float frizzTime) {
        this.frizzTime = frizzTime;
    }

    public float getFrostWeaponRange() {
        return frostWeaponRange;
    }

    public void setFrostWeaponRange(float frostWeaponRange) {
        this.frostWeaponRange = frostWeaponRange;
    }

    public void refresh() {
        hp = maxHp;
        underAttak = 0;
        fail = false;
        stoped = false;
        stateTime = 0;
        mana = maxMana;
        lightingCDCharge = lightingCD;
    }

    public boolean lightingIsCharged() {
        return lightingCD == lightingCDCharge;
    }

    public void rechargeLighting() {
        lightingCDCharge = 0;
    }

    public void setFireWeaponDamage(int fireWeaponDamage) {
        this.fireWeaponDamage = fireWeaponDamage;
    }

    public void setFireWeaponDamageDiff(int fireWeaponDamageDiff) {
        this.fireWeaponDamageDiff = fireWeaponDamageDiff;
    }

    public boolean isLightingReady() {
        return lightingCDCharge == lightingCD;
    }

    class UI extends Group
    {
        private final WeaponSwitcher weaponSwitcher;
        private final Texture life;
        private final Texture rageCounter;
        private final TextureRegion[] manaBar;
        private final TextureRegion[] rageBar;
        private TextureRegion[] lifeRegions;
        private float lifeHolderX;
        private float lifeHolderY;
        private float mul;
        private float manaConturX;
        private float manaConturY;
        private float rageConturX;
        private float rageConturY;
        private float i = 0;
        private Texture manaContur;
        private Texture lifeHolder;
        private int manaShift;
        private float manaBarX;

        public UI(PlayerActor player)
        {
            lifeHolderX = stage.getWidth() - 28 - Preloader.manager.get(Preloader.lifeHolder).getWidth();
            life = Preloader.manager.get(Preloader.life);
            lifeRegions = new TextureRegion[life.getHeight()];
            for (int i = 0; i < life.getHeight(); i++)
            {
                lifeRegions[lifeRegions.length - i - 1] = new TextureRegion(life, 0, i, life.getWidth(), life.getHeight() - i);
            }
            mul = lifeRegions.length - 1;
            lifeHolderY = 23;

            weaponSwitcher = new WeaponSwitcher(player);
            addActor(weaponSwitcher);
            weaponSwitcher.setX(33);
            weaponSwitcher.setY(20);

            manaConturX = 130;
            manaConturY = 13;

            rageCounter = Preloader.manager.get(Preloader.rageContur);
            rageConturX = stage.getWidth() - rageCounter.getWidth() - 125;
            rageConturY = 13;

            manaContur = Preloader.manager.get(Preloader.manaContur);

            lifeHolder = Preloader.manager.get(Preloader.lifeHolder);

            Texture manaTexture = Preloader.manager.get(Preloader.mana);
            manaBar = new TextureRegion[manaTexture.getWidth()];
            for (int i = 0; i < manaTexture.getWidth(); i++)
            {
                manaBar[i] = new TextureRegion(manaTexture, manaTexture.getWidth() - i - 1, 0, i, manaTexture.getHeight());
            }
            manaBarX = manaConturX + manaTexture.getWidth();


            Texture rageTexture = Preloader.manager.get(Preloader.rage);
            rageBar = new TextureRegion[manaTexture.getWidth()];
            for (int i = 0; i < rageTexture.getWidth(); i++)
            {
                rageBar[i] = new TextureRegion(rageTexture, 0, 0, i, rageTexture.getHeight());
            }
        }

        @Override
        public void draw(Batch batch, float parentAlpha) {
            
            frostKickSmashEffect.draw(batch, parentAlpha);

            // Health
            batch.draw(lifeRegions[Math.round((hp / maxHp) * mul)], lifeHolderX + 20, lifeHolderY + 22);


            manaShift = Math.round((mana / maxMana) * (manaBar.length - 1));
            batch.draw(manaBar[manaShift], manaBarX - manaShift, manaConturY);
            batch.draw(manaContur, manaConturX, manaConturY);


            batch.draw(rageBar[Math.round((lightingCDCharge / lightingCD) * (rageBar.length - 1))], rageConturX, rageConturY);
            batch.draw(rageCounter, rageConturX, rageConturY);


            batch.draw(lifeHolder, lifeHolderX, lifeHolderY);
            super.draw(batch, parentAlpha);

        }
    }
}
