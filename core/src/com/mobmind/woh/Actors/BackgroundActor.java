package com.mobmind.woh.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.mobmind.woh.Kernel;

public class BackgroundActor extends Actor {

	private Texture texture;

	public BackgroundActor(Texture texture){
		Kernel core = Kernel.getInstance();
		int width = core.getViewportWidth();
		int height = core.getViewportHeight();
        this.texture = texture;
		setBounds(0, 0, width, height);
		setOrigin(getWidth()/2, getHeight()/2);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		batch.disableBlending();
        batch.draw(texture, getX(), getY(), getWidth(), getHeight());
        batch.enableBlending();
	}
}
