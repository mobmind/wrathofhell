package com.mobmind.woh.Actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.mobmind.woh.Kernel;

public class DebugActor extends Actor{

	private Actor debugObject;
	private ShapeRenderer shape;

	public DebugActor(Actor a){
		debugObject = a;
		setBounds(debugObject.getX(), debugObject.getY(), debugObject.getWidth(), debugObject.getHeight());
		Kernel.getInstance().getStage().addActor(this);
		shape = new ShapeRenderer();
	}

	@Override
	public void draw(Batch batch, float parentAlpha){
		// получаем матрицу основного проекта
		final Matrix4 matrix = batch.getProjectionMatrix();
		super.draw(batch, parentAlpha);
		batch.end();   //  ** End the batch context
		shape.setProjectionMatrix(matrix);
		shape.begin(ShapeRenderer.ShapeType.Line);
		shape.setColor(1f, 0, 0, 0);
		shape.rect(getX(), getY(), getWidth(), getHeight());
		shape.end();
		batch.begin(); // ** Restart SpriteBatch context that caller assumes is active
	}

	@Override
	public void act(float delta) {
		if(debugObject.getY() != getY() && debugObject.getX() != getX()){
			setPosition(debugObject.getX(), debugObject.getY());
		}
	}
}