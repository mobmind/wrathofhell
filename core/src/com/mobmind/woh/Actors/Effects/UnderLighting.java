package com.mobmind.woh.Actors.Effects;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Tools.Pool;
import com.mobmind.woh.Tools.Preloader;

public class UnderLighting extends Actor {
	private final Pool pool = Pool.getInstance();
	private Actor target;
	private Animation anim;
	private float stateTime;
	private TextureRegion currentFrame;

	public void freeStateTime() { stateTime = 0; }
	public void setTarget(Actor target){
		this.target = target;
		setWidth(target.getWidth());
		setHeight(target.getHeight());
		setOrigin(target.getOriginX(), target.getOriginY());
	}

	public UnderLighting(){
		if(Kernel.getInstance().logLvl == Application.LOG_DEBUG) {
			pool.iterUnderLightingCount();
			Gdx.app.debug("underLightingCountPool", Integer.toString(pool.getUnderLightingCount()));
		}

		anim = Preloader.lightingCharge;
	}

	@Override
	public void act(float delta) {
		stateTime += delta;
		if (anim.isAnimationFinished(stateTime)){
			pool.worker(this);
		}
		currentFrame = anim.getKeyFrame(stateTime);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		batch.draw(
			currentFrame,
			target.getX() - getOriginX(),
			target.getY() - getOriginY(),
			getOriginX(), getOriginY(),
			getWidth(), getHeight(),
			getScaleX(), getScaleY(),
			target.getRotation()
		);
	}
}
