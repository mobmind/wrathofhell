package com.mobmind.woh.Actors.Effects;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.mobmind.woh.Actors.MobActor;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Tools.Pool;

public class LightingActor extends Group {

	private final Pool pool = Pool.getInstance();

	public LightingActor(Array<MobActor> markedMobs, Array<MobActor> affectedMobs) {
		Kernel core = Kernel.getInstance();
		addActor(createLighting(markedMobs.get(0), markedMobs.get(1)));
		addActor(createLighting(markedMobs.get(1), markedMobs.get(2)));
        addActor(createLighting(markedMobs.get(2), markedMobs.get(0)));
        core.getGameScreen().getBottomEffectsGroup().addActor(createUnderLighting(markedMobs.get(0)));
        core.getGameScreen().getBottomEffectsGroup().addActor(createUnderLighting(markedMobs.get(1)));
        core.getGameScreen().getBottomEffectsGroup().addActor(createUnderLighting(markedMobs.get(2)));

        for(MobActor mob : affectedMobs){
            addActor(
	            createLighting(
		            markedMobs.get(core.getRandomInt(3)), mob
	            )
	        );
        }
    }

	private UnderLighting createUnderLighting(Actor target){
		UnderLighting underLighting = pool.getUnderLights().obtain();
		underLighting.setTarget(target);
		return underLighting;
	}

	private Lighting createLighting(MobActor src, MobActor dst){
		Lighting lighting = pool.getLights().obtain();
		lighting.setSrcDst(src, dst);
		return lighting;
	}
}