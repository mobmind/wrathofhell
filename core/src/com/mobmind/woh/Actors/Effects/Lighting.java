package com.mobmind.woh.Actors.Effects;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Tools.Pool;
import com.mobmind.woh.Tools.Preloader;

public class Lighting extends Actor {
	private final Animation lightingAnim;
	private final Vector2 startOrigin;
	private final Pool pool = Pool.getInstance();
    private final Kernel core;
    private Actor src;
	private Actor dst;
	private Vector2 start;
	private float scale;
	private float r;
	private TextureRegion currentFrame;
	private float stateTime;
    private long soundId;

    public void freeStateTime() { stateTime = 0; soundId = 0;}
	public void setSrcDst(Actor src, Actor dst) {
		this.src = src;
		this.dst = dst;
        core.playSound(Preloader.manager.get(Preloader.sound_lighting_start));
	}

	public Lighting(){
		if(Kernel.getInstance().logLvl == Application.LOG_DEBUG) {
			pool.iterLightingCount();
			Gdx.app.debug("lightingCountPool", Integer.toString(pool.getLightingCount()));
		}

        core = Kernel.getInstance();

		lightingAnim = Preloader.lightingKick;
		startOrigin = new Vector2(143, 124);
	}

	@Override
	public void act(float delta) {
		stateTime += delta;
		if (lightingAnim.isAnimationFinished(stateTime)){
			pool.worker(this);
		}
		currentFrame = lightingAnim.getKeyFrame(stateTime, false);
        if ((soundId == 0) && (stateTime > 0.30f))
        {
            soundId = core.playSound(Preloader.manager.get(Preloader.sound_lighting));
        }
		start = new Vector2(src.getX(), src.getY());
		Vector2 end = new Vector2(dst.getX(), dst.getY());
		scale = start.dst(end) / 304; //расстояние берется между точами удара молнии
		r = end.cpy().sub(start).angle();
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		batch.draw(
			currentFrame,
			start.x - startOrigin.x,
			start.y - startOrigin.y,
			startOrigin.x,
			startOrigin.y,
			593, 254,
			scale, scale,
			r
		);
	}
}
