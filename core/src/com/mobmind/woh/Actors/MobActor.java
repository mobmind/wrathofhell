package com.mobmind.woh.Actors;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Json;
import com.mobmind.woh.Json.Mob.JsonMob;
import com.mobmind.woh.Json.Mob.Stats.Resist.JsonResist;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Screens.GameScreen;
import com.mobmind.woh.Tools.*;
import com.mobmind.woh.Tools.AI.*;

import java.lang.reflect.InvocationTargetException;

public class MobActor extends Actor{

	private final Kernel core = Kernel.getInstance();
	private final Pool pool = Pool.getInstance();
	private float attackSpeed;
    private Animation deathAnim;
    private Stage stage;
	/* координаты куда нам двигаться, собственно координаты самого игрока */
	private float targetX = Kernel.getInstance().getViewportWidth() / 2;
	private float targetY = Kernel.getInstance().getViewportHeight() / 2;
	private float speed = 0.2f;
	private float damage;
	private int HP;
	private float stateTime;
	private TextureRegion currentFrame;
	private AnimationDriver anim;
    private Animation runAnim;
    private Animation attackAnim;
    private float fireResist;
	private float coldResist;
	private float lightningResist;
	private float earthResist;
	private float x;
	private float y;
    //TODO Вынести в конфиг
    private float radius = 50;
    private double weaponRange;
    private MobState state;
    private FloatingText floatingText;
    private MobAI mobAI;

    private boolean dead;
    private Charge charge;

    private float damageToAdd;
    private float frizzTime;
    private MobState frizzedState;
	private boolean marked;
    private Sound chargeSound;
    private long chargeSoundId = 0;

    public boolean isMarked() { return marked; }

    public void setMarked(boolean marked) {
        this.marked = marked;
        if (marked) {
            stage.addActor(charge);
            if (chargeSoundId != 0) {
                core.stopSound(chargeSound, chargeSoundId);
            }
            chargeSoundId = core.loopSound(chargeSound);
        }else{
            core.stopSound(chargeSound, chargeSoundId);
            chargeSoundId = 0;
            charge.remove();
        }
    }

	public void freeStateTime(){ stateTime = 0; }
	public void freeState(){ state = MobState.Move; }

	public void setType(String type){
        chargeSound = Preloader.manager.get(Preloader.sound_charge);
		Json json = new Json();
        if (!Preloader.manager.isLoaded("Json/Mobs/" + type + ".json", JsonMob.class))
        {
            Preloader.manager.load("Json/Mobs/" + type + ".json", JsonMob.class);
            Preloader.manager.finishLoading();
        }
		JsonMob mob = Preloader.manager.get("Json/Mobs/" + type + ".json");
		/* Переосмыслить и переписать эту убогость */
		this.HP = mob.stats.HP;
		this.speed = mob.stats.speed;
		this.damage = mob.stats.damage;
		this.attackSpeed = mob.stats.attackSpeed;

		Texture sprite = Preloader.getMobByType(mob.texture);
        try {
            mobAI = (MobAI) Class.forName("com.mobmind.woh.Tools.AI." + mob.AI + "MobAI").getConstructor(MobActor.class).newInstance(this);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            mobAI = new SimpleMobAI(this);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }


        anim = new AnimationDriver(sprite, mob.anim.run.width, mob.anim.run.height);
		anim.setRunAnim(mob.anim.run.frameStart, mob.anim.run.frameEnd, mob.anim.run.frameTime / speed);
		anim.setAttackAnim(mob.anim.attack.frameStart, mob.anim.attack.frameEnd, mob.anim.attack.frameTime / attackSpeed);
		runAnim = new Animation(anim.getRunAnim().frameDuration, anim.getRunAnim().getKeyFrames() );
		attackAnim = new Animation(anim.getAttackAnim().frameDuration, anim.getAttackAnim().getKeyFrames() );
		deathAnim = anim.getAnimation(mob.anim.death.frameStart, mob.anim.death.frameEnd, mob.anim.death.frameTime);

		radius = mob.anim.run.radius;

		float width = mob.anim.run.width;
		float height = mob.anim.run.height;
		float props = height / width;
		setBounds(x, y, radius * 2, radius * 2 * props);
		setOrigin(getWidth() / 2, getHeight() / 2);

		currentFrame = runAnim.getKeyFrame(0);

		dead = false;
        damageToAdd = 0;
        frizzTime = 0;
		marked = false;
		charge = new Charge(this);

        for (JsonResist resist : mob.stats.resist) {
            switch (resist.type) {
                case Cold:
                    coldResist = resist.value;
                    break;
            }
        }
        state = MobState.Move;
        stateTime = 0;
		((GameScreen)core.getScreen()).getMobsGroup().addActor(this);
        setVisible(true);

        Difficulty.modify(this);
	}

	public MobActor(){
		if(core.logLvl == Application.LOG_DEBUG) {
			pool.iterMobCount();
			Gdx.app.debug("mobCountPool", Integer.toString(pool.getMobCount()));
		}

        stage = core.getStage();

        floatingText = new FloatingText(this);
        core.getStageUI().addActor(floatingText);
        state = MobState.Move;
		calculateParams();
        recalcWeaponRange(core.getPlayer().getFireWeaponRange());

		setName("mob");
	}

	@Override
	public void act(float delta) {
		stateTime += delta;

        mobAI.act(delta);

        if (state == MobState.Free)
        {
            if (floatingText.isFinished())
            {
                dispose();
            }else{
                return;
            }
        }

		if(state == MobState.Frizzed){
			frizzTime -= delta * coldResist;
			if (frizzTime <= 0){
				state = frizzedState;
				frizzTime = 0;
			}else {
                return;
            }
		}


		if(state == MobState.Move){
			updateAngle();
			currentFrame = runAnim.getKeyFrame(stateTime, true);
		}
		if(state == MobState.Dead){
            if (isVisible()) {
                if (deathAnim.isAnimationFinished(stateTime)) {
                    stage.addActor(this);
                    setVisible(false);
                    state = MobState.Free;
                } else {
                    currentFrame = deathAnim.getKeyFrame(stateTime, false);
                }
            }
		}
		if(state == MobState.Attack){
			if (attackAnim.isAnimationFinished(stateTime)){
				core.getPlayer().addDamage(damage);
				stateTime = 0;
			}
			currentFrame = attackAnim.getKeyFrame(stateTime, false);
		}
	}

    public void dispose() {
        setMarked(false);
        Pool.getInstance().worker(this);
    }

    @Override
    public boolean remove() {
        return super.remove();
    }

    private void updateAngle() {
		Vector2 from = new Vector2(getX(), getY());
		Vector2 to = new Vector2(targetX, targetY);
		float angle = to.sub(from).nor().angle();
		setRotation(angle - 90);
	}

	private  void updatePosition(float delta) {

	}

	private void calculateParams(){
		float xr = core.getViewportWidth() / 2 + radius;
		float yr = core.getViewportHeight() / 2 + radius;
		float a = (float) (core.getRandom() * Math.PI * 2);
		x = (float) (Math.cos(a) * xr) + 640;
		y = (float) (Math.sin(a) * yr) + 413;
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
        Color color = batch.getColor();
        if (state == MobState.Frizzed) {
            batch.setColor(Pallet.Frizzed);
        }
		batch.draw(currentFrame,
			getX() - getOriginX(), getY() - getOriginY(),
			getOriginX(), getOriginY(),
			getWidth(), getHeight(),
			getScaleX(), getScaleY(),
			getRotation()
		);
        batch.setColor(color);
	}

    public void recalcWeaponRange(float fireWeaponRange)
    {
        weaponRange = Math.pow( radius + fireWeaponRange, 2 );
    }

    public MobActor hit(float screenX, float screenY) {
        if (Math.pow(getX() - screenX, 2) + Math.pow(getY() - screenY, 2) < weaponRange){
            return this;
        } else {
            return null;
        }
    }

    public void addDamage(float damage) {
        if (state == MobState.Dead || state == MobState.Free) {return;}
        HP -= damage;
        damageToAdd += damage;
        floatingText.update(true);
        if (HP <= 0)
        {
            die(true);
        }
    }

    public void frizz(float time)
    {
        frizzTime += time;
        if (state != MobState.Frizzed) {
            frizzedState = state;
        }
        state = MobState.Frizzed;
    }

    public MobState getState() {
        return state;
    }

    public MobState getFrizzedState() {
        return frizzedState;
    }

    public int getMaxLife() {
        return HP;
    }

    public void setMaxLife(int maxLife) {
        this.HP = maxLife;
    }

    public float getSpeed() {
        return speed;
    }

    public float getTargetX() {
        return targetX;
    }

    public float getTargetY() {
        return targetY;
    }

    public void setState(MobState state) {
        this.state = state;
    }

    public void setStateTime(int stateTime) {
        this.stateTime = stateTime;
    }

    public Animation getDeathAnim() {
        return deathAnim;
    }

    public float getStateTime() {
        return stateTime;
    }

    public float getDamage() {
        return damage;
    }

    public void die(boolean killed) {
        stateTime = 0;
        state = MobState.Dead;
        core.getGameScreen().onMobDeath(this, killed);
    }

    class Charge extends Actor {

        private final Color color;
        float alpha;
        TextureRegion currentTexture;
        MobActor parent;
        float stateTime;
        float animTime;
        Kernel core;

        public Charge(MobActor parent){
            color = Color.WHITE;
            color.a = 0;
            setColor(color);
            this.parent = parent;
            animTime = 0.3f;
            core = Kernel.getInstance();
            currentTexture = Preloader.lightingEffect[core.getRandomInt(Preloader.lightingEffect.length)];
        }


        @Override
        public void act(float delta) {
            stateTime += delta;
            if (stateTime >= animTime)
            {
                stateTime = stateTime % animTime; // обрежем до излишков чтобы не накапливать действия при простое
                currentTexture = Preloader.lightingEffect[core.getRandomInt(Preloader.lightingEffect.length)];
            }
            color.a = Interpolation.bounce.apply((stateTime % 0.3f) / 0.3f);

        }

        @Override
        public void draw(Batch batch, float parentAlpha) {
            Color tmp = batch.getColor();
            batch.setColor(color);
            batch.draw(currentTexture, parent.getX() - parent.getOriginX(), parent.getY() - parent.getOriginY(), parent.getOriginX(), parent.getOriginY(), parent.getWidth(), parent.getHeight(), parent.getScaleX(), parent.getScaleY(), parent.getRotation());
            batch.setColor(tmp);
        }
    }

    class FloatingText extends Actor
    {
        private final Actor owner;
        private BitmapFont damageText;
        private float damageToShow;
        private int damageRank = 1;
        private float damageTextX;
        private float damageTextY = 30;
        private final float damageShowSpeed = 2f;
        private Color color;
        private boolean finished;

        public boolean isFinished()
        {
            return finished;
        }

        public void update(boolean visible) {
            if (visible)
            {
                color.a = 1;
                finished = false;
            }
            super.setVisible(visible);
        }

        FloatingText(Actor owner)
        {
            this.owner = owner;
            damageText = Preloader.damageFont22;
            color = Color.BLACK.cpy();
            color.a = 0;
            finished = true;
            //setName(String.valueOf(Kernel.getInstance().getRandomInt(10000)));
        }

        @Override
        public void act(float delta) {
            if (finished) { return; }
            if (damageToAdd == 0) {
                color.a -= delta;
                if (color.a < 0)
                {
                    color.a = 0;
                    damageToShow = 0;
                    damageRank = 1;
                    finished = true;
                }
            }else{
                float i = delta * damageShowSpeed * (damageToAdd > 5 ? damageToAdd : 5); //TODO рассчет этого параметра на основе не показанного урона
                if (damageToAdd > i) {
                    damageToShow += i;
                    damageToAdd -= i;
                } else {
                    damageToShow += damageToAdd;
                    damageToAdd = 0;
                }
            }
            if (damageToShow >= 1) {
                setScale(1 + damageToShow / 100);
                damageText.setScale(getScaleX());
                damageTextX = - damageText.getBounds(String.valueOf((int) Math.round(damageToShow))).width / 2;
                setPosition(owner.getX() + damageTextX, owner.getY() + damageTextY * getScaleX());
            }
        }

        @Override
        public void draw(Batch batch, float parentAlpha) {
            if (damageToShow >= 1) {
                damageText.setColor(color);
                damageText.setScale(getScaleX());
                damageText.draw(batch, String.valueOf((int) Math.round(damageToShow)), getX(), getY());
            }
        }
    }
}
