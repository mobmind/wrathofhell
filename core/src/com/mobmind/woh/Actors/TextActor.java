package com.mobmind.woh.Actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFontCache;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * Created by seedofpanic on 20.07.2014.
 */
public class TextActor extends Actor {
    private boolean centerX;
    private boolean centerY;
    private BitmapFontCache font;
    public TextActor(BitmapFontCache cachedFont)
    {
        this.font = cachedFont;
    }

    public void setText(CharSequence str, float x, float y, boolean centerX, boolean centerY)
    {
        this.centerX = centerX;
        this.centerY = centerY;
        setPosition(x, y);
        BitmapFont.TextBounds bounds = font.setText(str, x, y);
        font.setPosition( centerX ? - bounds.width / 2 : 0, centerY ? - bounds.height / 2 : 0);
    }

    @Override
    public float getHeight() {
        return font.getBounds().height;
    }

    public void setText(CharSequence str)
    {
         setText(str, getX(), getY(), centerX, centerY);
    }

    public BitmapFontCache getFont()
    {
        return font;
    }

    @Override
    public void setPosition(float x, float y) {
        font.setPosition(x, y);
        super.setPosition(x, y);
    }

    @Override
    public void setX(float x) {
        font.setPosition(x, font.getY());
        super.setX(x);
    }

    @Override
    public void setY(float y) {
        font.setPosition(font.getY(), y);
        super.setY(y);
    }

    @Override
    public void setBounds(float x, float y, float width, float height) {
        font.setPosition(x, y);
        super.setBounds(x, y, width, height);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        font.draw(batch);
    }
}
