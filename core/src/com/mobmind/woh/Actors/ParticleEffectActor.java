package com.mobmind.woh.Actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by seedofpanic on 31.07.2014.
 */
public class ParticleEffectActor extends Actor {
    public ParticleEffect effect;

    public ParticleEffectActor(ParticleEffect effect) {
        this.effect = effect;
        effect.allowCompletion();
        effect.start(); //need to start the particle spawning
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        effect.draw(batch); //define behavior when stage calls Actor.draw()
    }

    public void act(float delta) {
        super.act(delta);
        effect.setPosition(getX(), getY()); //set to whatever x/y you prefer
        effect.update(delta); //update it

        if (effect.isComplete())
        {
            getParent().removeActor(this);
        }
    }

    public ParticleEffect getEffect() {
        return effect;
    }
}
