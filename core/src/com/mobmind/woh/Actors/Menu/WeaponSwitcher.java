package com.mobmind.woh.Actors.Menu;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.mobmind.woh.Actors.PlayerActor;
import com.mobmind.woh.Tools.Preloader;
import com.mobmind.woh.Tools.RadialFiller;
import com.mobmind.woh.Tools.Weapon;

/**
 * Created by seedofpanic on 07.08.2014.
 */
public class WeaponSwitcher extends Actor {
    final PlayerActor player;
    private int i = 0;
    private Texture icon_fire;
    private Texture icon_cold;
    private Texture icon_lighting;

    public WeaponSwitcher(final PlayerActor player)
    {
        icon_fire = Preloader.manager.get(Preloader.icon_fire);
        icon_cold = Preloader.manager.get(Preloader.icon_cold);
        icon_lighting = Preloader.manager.get(Preloader.icon_lighting);

        this.player = player;
        addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (x < 200 && y < 200) {
                    switch (player.getWeapon())
                    {
                        case Fire:
                            player.setWeapon(Weapon.Type.Frost);
                            break;
                        case Frost:
                            if (player.isLightingReady()) {
                                player.setWeapon(Weapon.Type.Light);
                            }else{
                                player.setWeapon(Weapon.Type.Fire);
                            }
                            break;
                        case Light:
                            player.setWeapon(Weapon.Type.Fire);
                            break;
                    }
                    return true;
                }
                return false;
            }
        });
        setWidth(200);
        setHeight(200);

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        // Weapons
        batch.draw(icon_fire, getX(), getY());
        if (player.getWeapon() == Weapon.Type.Frost)
        {
            batch.draw(icon_cold, getX() + 23, getY() + 23);
        }else if (player.getWeapon() == Weapon.Type.Light)
        {
            batch.draw(icon_lighting, getX() + 23, getY() + 23);
        }
    }
}
