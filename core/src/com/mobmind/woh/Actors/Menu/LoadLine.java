package com.mobmind.woh.Actors.Menu;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by seedofpanic on 20.08.2014.
 */
public class LoadLine extends Actor {
    final NinePatch ninePatch;

    public LoadLine(Texture texture, float x, float y, int left, int right, int top, int bottom) {
        ninePatch = new NinePatch(texture, left, right, top, bottom);

        setPosition(x, y);
        setHeight(texture.getHeight());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        ninePatch.draw(batch, getX(), getY(), getWidth() + ninePatch.getRightWidth(), getHeight());
    }
}
