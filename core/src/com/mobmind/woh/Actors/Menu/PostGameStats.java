package com.mobmind.woh.Actors.Menu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Screens.GameScreen;
import com.mobmind.woh.Tools.AnimationDriver;
import com.mobmind.woh.Tools.Preloader;

/**
 * Created by seedofpanic on 10.08.2014.
 */
public class PostGameStats extends ModalGroup {
    private final Table winTable;
    private final Button nextButton;
    private final Table loseTable;
    private final VictoryStar victoryStar1;
    private final VictoryStar victoryStar2;
    private final VictoryStar victoryStar3;
    private final Animation victoryStarAnim;
    private final Table starsTable;
    private final Animation victoryGotStarAnim;
    private final Label totalKilledLabel;
    private final Label totalScoreLabel;
    private final Kernel core;
    private final Animation victoryGotStarStartAnim;
    private float stateTime = 0;

    public PostGameStats(final GameScreen gameScreen) {
        core = Kernel.getInstance();

        winTable = new Table();
        //winTable.debug();
        Texture winBackTexture = Preloader.manager.get(Preloader.sheetOfVictory);
        winTable.align(Align.center);
        winTable.setBackground(new TextureRegionDrawable(new TextureRegion(winBackTexture)));
        winTable.setWidth(winBackTexture.getWidth());
        winTable.setHeight(winBackTexture.getHeight());
        winTable.setPosition(core.getStage().getWidth() / 2 - winTable.getWidth() / 2, core.getStage().getHeight() / 2 - winTable.getHeight() / 2);

        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = Preloader.mainFont18;
        labelStyle.fontColor = Color.BLACK;

        winTable.add().colspan(2).height(220).row();

        Array<Integer> startKeyFrames = new Array<Integer>();
        startKeyFrames.add(0);startKeyFrames.add(0);startKeyFrames.add(0);startKeyFrames.add(0);startKeyFrames.add(0);
        startKeyFrames.add(1);startKeyFrames.add(1);startKeyFrames.add(1);startKeyFrames.add(1);startKeyFrames.add(1);
        startKeyFrames.add(2);startKeyFrames.add(2);startKeyFrames.add(2);startKeyFrames.add(2);startKeyFrames.add(2);
        startKeyFrames.add(3);startKeyFrames.add(3);
        startKeyFrames.add(4);startKeyFrames.add(4);startKeyFrames.add(4);startKeyFrames.add(4);startKeyFrames.add(4);
        startKeyFrames.add(3);startKeyFrames.add(3);


        Array<Integer> keyFrames = new Array<Integer>();

        keyFrames.add(5);keyFrames.add(5);
        keyFrames.add(6);keyFrames.add(6);keyFrames.add(6);keyFrames.add(6);keyFrames.add(6);keyFrames.add(6);keyFrames.add(6);
        keyFrames.add(7);keyFrames.add(7);keyFrames.add(7);keyFrames.add(7);keyFrames.add(7);keyFrames.add(7);keyFrames.add(7);
        keyFrames.add(8);keyFrames.add(8);keyFrames.add(8);keyFrames.add(8);keyFrames.add(8);keyFrames.add(8);keyFrames.add(8);
        keyFrames.add(9);keyFrames.add(9);keyFrames.add(9);keyFrames.add(9);keyFrames.add(9);keyFrames.add(9);keyFrames.add(9);
        keyFrames.add(8);keyFrames.add(8);keyFrames.add(8);keyFrames.add(8);keyFrames.add(8);keyFrames.add(8);keyFrames.add(8);
        keyFrames.add(7);keyFrames.add(7);keyFrames.add(7);keyFrames.add(7);keyFrames.add(7);keyFrames.add(7);keyFrames.add(7);
        keyFrames.add(6);keyFrames.add(6);keyFrames.add(6);keyFrames.add(6);keyFrames.add(6);keyFrames.add(6);keyFrames.add(6);



        AnimationDriver starAnimationDriver = new AnimationDriver(Preloader.manager.get(Preloader.victoryStar), 122, 115);
        victoryStarAnim = starAnimationDriver.getAnimation(11, 14, 0.2f);
        victoryGotStarAnim = starAnimationDriver.getAnimation(keyFrames, 0.02f);
        victoryGotStarStartAnim = starAnimationDriver.getAnimation(startKeyFrames, 0.02f);

        victoryStar1 = new VictoryStar();
        victoryStar1.setGot(true);
        victoryStar2 = new VictoryStar();
        victoryStar3 = new VictoryStar();
        starsTable = new Table();
        starsTable.add(victoryStar1).padRight(20);
        starsTable.add(victoryStar2).padRight(20);
        starsTable.add(victoryStar3);
        winTable.add(starsTable).colspan(2).row();

        totalKilledLabel = new Label("0", labelStyle);
        totalScoreLabel = new Label("0", labelStyle);
        addStatCell(winTable, new Label("Total killed", labelStyle), totalKilledLabel);
        addStatCell(winTable, new Label("Total score", labelStyle), totalScoreLabel);

        Table winButtonsTable = new Table();

        Button.ButtonStyle forwardButtonStyle = new Button.ButtonStyle();
        forwardButtonStyle.up = new TextureRegionDrawable(new TextureRegion(Preloader.manager.get(Preloader.icon_home)));
        Button homeButton = new Button(forwardButtonStyle);
        homeButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.playSound(Preloader.manager.get(Preloader.sound_click));
                core.showScreen(core.getLevelSelectScreen());
            }
        });
        winButtonsTable.add(homeButton).padRight(30);

        Button.ButtonStyle restartButtonStyle = new Button.ButtonStyle();
        restartButtonStyle.up = new TextureRegionDrawable(new TextureRegion(Preloader.manager.get(Preloader.icon_restart1)));
        Button restartButton = new Button(restartButtonStyle);
        restartButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.playSound(Preloader.manager.get(Preloader.sound_click));
                gameScreen.restart();
            }
        });
        winButtonsTable.add(restartButton).padRight(25);

        Button.ButtonStyle nextButtonStyle = new Button.ButtonStyle();
        nextButtonStyle.up = new TextureRegionDrawable(new TextureRegion(Preloader.manager.get(Preloader.icon_play_next)));
        nextButton = new Button(nextButtonStyle);
        nextButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.playSound(Preloader.manager.get(Preloader.sound_click));
                core.getGameScreen().playNextLevel();
            }
        });
        winButtonsTable.add(nextButton);

        winTable.add(winButtonsTable).padTop(20).colspan(2);
        winTable.add().padRight(40);
        addActor(winTable);

        loseTable = new Table();
        Texture loseBackTexture = Preloader.manager.get(Preloader.sheetOfLose);
        loseTable.align(Align.center);
        loseTable.setBackground(new TextureRegionDrawable(new TextureRegion(loseBackTexture)));
        loseTable.setWidth(loseBackTexture.getWidth());
        loseTable.setHeight(loseBackTexture.getHeight());
        loseTable.setPosition(core.getStage().getWidth() / 2 - loseTable.getWidth() / 2, core.getStage().getHeight() / 2 - loseTable.getHeight() / 2);

        Button homeButtonLose = new Button(forwardButtonStyle);
        homeButtonLose.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.playSound(Preloader.manager.get(Preloader.sound_click));
                core.showScreen(core.getLevelSelectScreen());
            }
        });

        Button restartButtonLose = new Button(restartButtonStyle);
        restartButtonLose.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.playSound(Preloader.manager.get(Preloader.sound_click));
                gameScreen.restart();
            }
        });

        loseTable.add(homeButtonLose).padRight(120).padTop(70);
        loseTable.add(restartButtonLose).padTop(70);

        gameScreen.getStageUI().addActor(this);
        setVisible(false);
    }

    public void setStars(int count)
    {
        if (count == 3) {
            victoryStar3.setGot(true);
        }else{
            victoryStar3.setGot(false);
        }
        if (count > 1)
        {
            victoryStar2.setGot(true);
        }else{
            victoryStar2.setGot(false);
        }
        if (count > 0)
        {
            victoryStar1.setGot(true);
        }else{
            victoryStar1.setGot(false);
        }
    }

    private void addStatCell(Table table, Label name, Label value) {
        table.add(name).align(Align.left).padLeft(15).height(40);
        table.add(value).align(Align.right).padRight(10).height(40).row();
    }


    @Override
    public void setVisible(boolean visible) {
        Kernel.getInstance().getDialogBack().setVisible(visible);
        if (visible)
        {
            if (Kernel.getInstance().getGameScreen().isVictory()) {
                winTable.layout();
                loseTable.remove();
                addActor(winTable);
                totalKilledLabel.setText(String.valueOf(core.getGameScreen().getTotalKilled()));
                totalScoreLabel.setText(String.valueOf(core.getGameScreen().getTotalScore()));
            }else{
                loseTable.layout();
                winTable.remove();
                addActor(loseTable);
            }
            stateTime = 0;
        }
        super.setVisible(visible);
    }

    @Override
    public void act(float delta) {
        stateTime += delta;
        super.act(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }

    private class VictoryStar extends Actor {

        private boolean got = false;
        private boolean started = false;

        public void setGot(boolean got)
        {
            this.got = got;
            started = false;
        }

        VictoryStar()
        {
            setWidth(122);
            setWidth(115);
        }

        @Override
        public void draw(Batch batch, float parentAlpha) {
            if (got)
            {
                if (victoryGotStarStartAnim.isAnimationFinished(stateTime))
                {
                    started = true;
                }
                if (started) {
                    batch.draw(victoryGotStarAnim.getKeyFrame(stateTime, true), getX(), getY());
                }else{
                    batch.draw(victoryGotStarStartAnim.getKeyFrame(stateTime, false), getX(), getY());
                }
            }else {
                batch.draw(victoryStarAnim.getKeyFrame(stateTime, true), getX(), getY());
            }
        }
    }
}
