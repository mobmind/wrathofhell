package com.mobmind.woh.Actors.Menu;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Array;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Tools.AnimationDriver;
import com.mobmind.woh.Tools.Preloader;

/*
Как вариант наследовать от Image из Tablelayout
т.к. он тоже является актером или от тейбл с
присвоением бекграунда
*/
public class StartBtn extends Actor {

	private Texture texutre;
	private Kernel core;
	private Stage stage;
    private Animation anim;
    private float stateTime;
    private TextureRegion currentFrame;

    public StartBtn() {
        stateTime = 0;
		core = Kernel.getInstance();
		stage = core.getStage();
		texutre = Preloader.manager.get(Preloader.startBtn);
        AnimationDriver animationDriver = new AnimationDriver(texutre, 735, 171);
        Array<Integer> keyFrames = new Array<Integer>();
        keyFrames.add(0);
        keyFrames.add(1);
        keyFrames.add(2);
        keyFrames.add(1);
        anim = animationDriver.getAnimation(keyFrames, 0.2f);
		setBounds(280, stage.getHeight() - 92, 735, 171);
		stage.addActor(this);
        setTouchable(Touchable.disabled);
	}

    @Override
    public void act(float delta) {
        stateTime += delta;
        currentFrame = anim.getKeyFrame(stateTime, true);
    }

    @Override
	public void draw(Batch batch, float parentAlpha) {

		batch.draw(
			currentFrame,
			getX(), getY(),
            getOriginX(), getOriginY(),
			getWidth(), getHeight(),
            1f, 1f, 0f

		);
	}
}
