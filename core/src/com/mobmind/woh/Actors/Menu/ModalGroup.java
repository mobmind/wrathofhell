package com.mobmind.woh.Actors.Menu;

import com.mobmind.woh.Kernel;

/**
 * Created by seedofpanic on 09.08.2014.
 */
public class ModalGroup extends com.badlogic.gdx.scenes.scene2d.Group {
    @Override
    public void setVisible(boolean visible) {
        Kernel.getInstance().getDialogBack().setVisible(visible);
        toFront();
        super.setVisible(visible);
    }
}
