package com.mobmind.woh.Actors.Menu;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Tools.AnimButton;
import com.mobmind.woh.Tools.Pallet;
import com.mobmind.woh.Tools.Preloader;

/**
 * Created by seedofpanic on 14.08.2014.
 */
public class MessageBox extends Table{
    private final Label label;
    private final Kernel core;

    public MessageBox(ChangeListener changeListener)
    {
        core = Kernel.getInstance();
        setBackground(new NinePatchDrawable(new NinePatch(Preloader.manager.get(Preloader.window9p))));

        Label.LabelStyle style = new Label.LabelStyle();
        style.font =  Preloader.mainFont22;
        style.fontColor = Pallet.Brown;
        label = new Label("", style);
        label.setAlignment(Align.center);
        align(Align.center);
        add(label).pad(10, 10, 10, 10).row();

        AnimButton okButton = new AnimButton(Preloader.manager.get(Preloader.okBtn), 0, 0, 133, 60, core.getMenuScreen().animBtnKeyFrames, 0.04f);
        okButton.addListener(changeListener);
        align(Align.center);
        add(okButton).pad(10, 10, 10, 10);
        okButton.setSoundEffect(Preloader.manager.get(Preloader.sound_ignite));
        setVisible(false);


    }

    @Override
    public void setVisible(boolean visible) {
        Kernel.getInstance().getDialogBack().setVisible(visible);
        toFront();
        super.setVisible(visible);
    }

    public void setMessage(String message)
    {
        label.setText(message);
        label.layout();

        setWidth(getPrefWidth());
        setHeight(getPrefHeight());
        setPosition(core.getStageUI().getWidth() / 2 - getPrefWidth() / 2, core.getStageUI().getHeight() / 2 - getHeight() / 2);
    }
}
