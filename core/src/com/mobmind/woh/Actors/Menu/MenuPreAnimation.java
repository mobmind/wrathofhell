package com.mobmind.woh.Actors.Menu;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Screens.MenuScreen;
import com.mobmind.woh.Tools.AnimationDriver;
import com.mobmind.woh.Tools.Preloader;

/**
 * Created by seedofpanic on 18.07.2014.
 */
public class MenuPreAnimation extends Actor {
    private final Animation fallAnim;
    private final Animation rollHeaderAnim;
    private final Animation staticHeaderAnim;
    private final Animation cicleHeaderAnim;
    private Texture texutre;
    private Kernel core;
    private Stage stage;
    private Animation rollAnim;
    private Animation staticAnim;
    private float stateTime;
    private boolean animFinished;
    private TextureRegion currentFrame;
    private TextureRegion currentHeaderFrame;
    private AnimState animState;
    private boolean headerCicled;

    public MenuPreAnimation() {
        stateTime = 0;
        core = Kernel.getInstance();
        stage = core.getStage();
        AnimationDriver animationDriver = new AnimationDriver(Preloader.menuPreAnims);
        float a = 50;

        fallAnim = animationDriver.getAnimation(1, 9, 0.1f);
        rollAnim = animationDriver.getAnimation(10, 17, 0.1f);
        Array<Integer> keyFrames = new Array<Integer>();
        keyFrames.add(17);
        keyFrames.add(18);
        keyFrames.add(19);
        keyFrames.add(18);

        staticAnim = animationDriver.getAnimation(keyFrames, 0.2f);

        //Header
        AnimationDriver headerAnimationDriver = new AnimationDriver(Preloader.menuHeader);
        rollHeaderAnim = headerAnimationDriver.getAnimation(1, 3, 0.1f);
        staticHeaderAnim = headerAnimationDriver.getAnimation(4, 21, 0.1f);
        keyFrames.clear();
        keyFrames.add(5);
        keyFrames.add(4);
        keyFrames.add(5);
        keyFrames.add(6);
        cicleHeaderAnim = headerAnimationDriver.getAnimation(keyFrames, 0.2f);

        setBounds(275, 55, 734, 950);
        stage.addActor(this);
        animState = AnimState.Fall;
        currentFrame = fallAnim.getKeyFrame(0, false);
    }

    @Override
    public void act(float delta) {
        if (isVisible()) {
            stateTime += delta;
            switch (animState) {
                case Static:
                    currentFrame = staticAnim.getKeyFrame(stateTime, true);
                    if (staticHeaderAnim.isAnimationFinished(stateTime))
                    {
                        stateTime = 0;
                        headerCicled = true;
                    }
                    if (headerCicled) {
                        currentHeaderFrame = cicleHeaderAnim.getKeyFrame(stateTime, true);
                    }else{
                        currentHeaderFrame = staticHeaderAnim.getKeyFrame(stateTime, false);
                    }
                    break;
                case Fall:
                    if (fallAnim.isAnimationFinished(stateTime)) {
                        animState = AnimState.Roll;
                        core.playSound(Preloader.manager.get(Preloader.sound_scroll_roll));
                        stateTime = 0;
                    }else {
                        currentFrame = fallAnim.getKeyFrame(stateTime, false);
                    }
                    break;
                case Roll:
                    if (rollAnim.isAnimationFinished(stateTime)) {
                        animState = AnimState.Static;
                        stateTime = 0;
                        ((MenuScreen) core.getMenuScreen()).switchMenuPage(((MenuScreen) core.getMenuScreen()).getSelectProfileGroup());
                    }else {
                        currentFrame = rollAnim.getKeyFrame(stateTime, false);
                        if (stateTime - 0.4f > 0) {
                            currentHeaderFrame = rollHeaderAnim.getKeyFrame(stateTime - 0.4f, false);
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {

        batch.draw(
                currentFrame,
                getX(), getY(),
                getOriginX(), getOriginY(),
                currentFrame.getRegionWidth(), currentFrame.getRegionHeight(),
                1f, 1f, 0f
        );
        if (currentHeaderFrame != null) {
            batch.draw(
                    currentHeaderFrame,
                    193, 487,
                    getOriginX(), getOriginY(),
                    currentHeaderFrame.getRegionWidth(), currentHeaderFrame.getRegionHeight(),
                    1f, 1f, 0f
            );
        }

    }

    enum AnimState{
        Fall, Roll, Static
    }
}
