package com.mobmind.woh.Actors.Menu;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by seedofpanic on 24.08.2014.
 */
public class AnimatedActor extends Actor {
    private final boolean cycled;
    Animation animation;
    private float stateTime = 0;

    public AnimatedActor(Animation animation, boolean cycled) {
        this.animation = animation;
        setWidth(animation.getKeyFrame(0).getRegionWidth());
        setHeight(animation.getKeyFrame(0).getRegionHeight());
        this.cycled = cycled;
    }

    @Override
    public void act(float delta) {
        stateTime += delta;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(animation.getKeyFrame(stateTime, cycled), getX(), getY(), getWidth(), getHeight());
    }
}
