package com.mobmind.woh.Actors.Menu;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Tools.AnimButton;
import com.mobmind.woh.Tools.Preloader;

/**
 * Created by seedofpanic on 09.08.2014.
 */
public class OpenSkillsButtonActor extends Group {
    private final float buttonHolderX;
    private final float buttonHolderY;
    private final AnimButton button;
    private Texture borderOfButtonMenu;

    public OpenSkillsButtonActor() {
        final Kernel core = Kernel.getInstance();
        borderOfButtonMenu = Preloader.manager.get(Preloader.borderOfButtonMenu);
        buttonHolderX = 10 ;
        buttonHolderY = core.getStage().getHeight() - 10 - Preloader.manager.get(Preloader.borderOfButtonPause).getHeight();
        Array<Integer> keyFrames = new Array<Integer>();
        keyFrames.add(0);
        keyFrames.add(1);
        keyFrames.add(2);
        keyFrames.add(3);
        keyFrames.add(4);
        Texture menuTexture = Preloader.manager.get(Preloader.icon_button_menu);
        button = new AnimButton(menuTexture, buttonHolderX - 1, buttonHolderY + 15, menuTexture.getWidth() / 5, menuTexture.getHeight(), keyFrames, 0.04f);
        //setBounds(button.getX(), button.getY(), button.getWidth(), button.getHeight());
        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.getSkillsScreen().setBackScreen(core.getGameScreen());
                core.showScreen(core.getSkillsScreen());
            }
        });
        addActor(button);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(borderOfButtonMenu, buttonHolderX, buttonHolderY);
        super.draw(batch, parentAlpha);
    }
}
