package com.mobmind.woh.Actors.Menu;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Tools.AnimButton;
import com.mobmind.woh.Tools.Preloader;

/**
 * Created by seedofpanic on 09.08.2014.
 */
public class PauseButtonActor extends Group{
    private final float pauseButtonHolderX;
    private final float pauseButtonHolderY;
    private final AnimButton pause;
    private final Texture borderOfButtonPause;

    public PauseButtonActor()
    {
        final Kernel core = Kernel.getInstance();
        borderOfButtonPause = Preloader.manager.get(Preloader.borderOfButtonPause);
        pauseButtonHolderX = core.getStage().getWidth() - 10 - borderOfButtonPause.getWidth();
        pauseButtonHolderY = core.getStage().getHeight() - 10 - borderOfButtonPause.getHeight();
        Array<Integer> keyFrames = new Array<Integer>();
        keyFrames.add(0);
        keyFrames.add(1);
        keyFrames.add(2);
        keyFrames.add(3);
        keyFrames.add(4);
        Texture pauseTexture = Preloader.manager.get(Preloader.icon_button_pause);
        pause = new AnimButton(pauseTexture, pauseButtonHolderX + 96, pauseButtonHolderY + 18, pauseTexture.getWidth() / 5, pauseTexture.getHeight(), keyFrames,0.04f);
        //setBounds(pause.getX(), pause.getY(), pause.getWidth(), pause.getHeight());
        pause.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (Kernel.paused) {
                    core.getGameScreen().unpause();
                } else {
                    core.getGameScreen().pause();
                }
            }
        });
        addActor(pause);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(borderOfButtonPause, pauseButtonHolderX, pauseButtonHolderY);
        super.draw(batch, parentAlpha);
    }
}
