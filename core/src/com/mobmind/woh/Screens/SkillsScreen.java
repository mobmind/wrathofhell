package com.mobmind.woh.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobmind.woh.Actors.BackgroundActor;
import com.mobmind.woh.Json.JsonLevel;
import com.mobmind.woh.Json.User.JsonProfile;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Templates.TemplateScreen;
import com.mobmind.woh.Tools.AnimationDriver;
import com.mobmind.woh.Tools.Preloader;

/**
 * Created by seedofpanic on 26.07.2014.
 */
public class SkillsScreen implements TemplateScreen {

    private Stage stage;
    private Stage stageUI;
    private Kernel core;
    private StretchViewport viewport;
    private BackgroundActor background;
    private InputMultiplexer inputMultiplexer;
    private boolean built;
    private TemplateScreen backScreen;

    @Override
    public Stage getStage() {
        return stage;
    }

    @Override
    public Stage getStageUI() {
        return stageUI;
    }

    @Override
    public void render(float v) {

        stage.act(v);
        stage.draw();

        stageUI.act(v);
        stageUI.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update();
    }

    @Override
    public void show() {
        if (built)
        {
            refresh();
        }else{
            stage.clear();
            stageUI.clear();
            build();
            built = true;
        }
    }

    public void refresh()
    {
        Gdx.input.setInputProcessor(inputMultiplexer);
        stageUI.addActor(Kernel.getInstance().getDialogBack());
        core.getDialogBack().setVisible(false);

    }

    public SkillsScreen()
    {
        core = Kernel.getInstance();

        viewport = new StretchViewport(Kernel.getViewportWidth(), Kernel.getViewportHeight());
        stage = new Stage(viewport);
        stageUI = new Stage(viewport);
        stageUI.addActor(core.getDialogBack());
        inputMultiplexer = new InputMultiplexer(stageUI, stage);
    }

    public void build()
    {


        initComponents();

        refresh();
    }

    private void initComponents() {
        background = new BackgroundActor(Preloader.manager.get(Preloader.skillsbg));
        stage.addActor(background);

        //BACK button
        Button.ButtonStyle backStyle = new Button.ButtonStyle();
        backStyle.up = new SpriteDrawable(new Sprite(Preloader.manager.get(Preloader.icon_back)));
        Button back = new Button(backStyle);
        back.setPosition(50, stage.getHeight() - 50 - back.getHeight());

        back.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.playSound(Preloader.manager.get(Preloader.sound_click));
                core.showScreen(backScreen);
            }
        });
        stage.addActor(back);

        Image commingsoon = new Image(Preloader.manager.get(Preloader.commingsoon));
        commingsoon.setPosition(stage.getWidth() / 2 - commingsoon.getWidth() / 2, stage.getHeight() - commingsoon.getHeight());
        stage.addActor(commingsoon);

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

    public void setBuilt(boolean built) {
        this.built = built;
    }

    public void setBackScreen(TemplateScreen backScreen) {
        this.backScreen = backScreen;
    }
}
