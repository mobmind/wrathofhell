package com.mobmind.woh.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Templates.TemplateScreen;

public class LevelScreen implements TemplateScreen {

	private Stage stage;
	private Kernel core;
	private ScreenViewport viewport;

	@Override
	public Stage getStage() {
		return stage;
	}

	@Override
	public Stage getStageUI() {
		return null;
	}

	@Override
	public void render(float v) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT | GL30.GL_DEPTH_BUFFER_BIT);

		stage.act(v);
		stage.draw();
	}

	@Override
	public void resize(int i, int i2) {
		viewport.update();
	}

	@Override
	public void show() {
		Gdx.app.debug("Screen", "Level");
		core = Kernel.getInstance();
		viewport = new ScreenViewport();
		stage = new Stage(viewport);
		Gdx.input.setInputProcessor(stage);
//		new Level("1");
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		stage.dispose();
	}
}
