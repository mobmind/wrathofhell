package com.mobmind.woh.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobmind.woh.Actors.BackgroundActor;
import com.mobmind.woh.Actors.Menu.OpenSkillsButtonActor;
import com.mobmind.woh.Json.JsonLevel;
import com.mobmind.woh.Json.User.JsonProfile;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Templates.TemplateScreen;
import com.mobmind.woh.Tools.AnimationDriver;
import com.mobmind.woh.Tools.Preloader;

/**
 * Created by seedofpanic on 26.07.2014.
 */
public class LevelSelectScreen implements TemplateScreen {

    private Stage stage;
    private Stage stageUI;
    private Kernel core;
    private StretchViewport viewport;
    private BackgroundActor background;
    private Array<JsonLevel> levels;
    private Image levelsNails;
    private Group levelGroup;
    private Table prfsWindow;
    private InputMultiplexer inputMultiplexer;
    private boolean built;
    private Animation star;
    private Animation emptyStar;

    @Override
    public Stage getStage() {
        return stage;
    }

    @Override
    public Stage getStageUI() {
        return stageUI;
    }

    @Override
    public void render(float v) {

        stage.act(v);
        stage.draw();

        stageUI.act(v);
        stageUI.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update();
    }

    @Override
    public void show() {
        if (!Preloader.menuLoaded) {
            core.getLoadingScreen().setNextScreen(this);
            core.showScreen(core.getLoadingScreen());
            Preloader.loadMenuData();
            return;
        }
        if (built)
        {
            refresh();
        }else{
            stage.clear();
            stageUI.clear();
            build();
            built = true;
        }
    }

    public void refresh()
    {
        Gdx.input.setInputProcessor(inputMultiplexer);
        stageUI.addActor(Kernel.getInstance().getDialogBack());
        core.getDialogBack().setVisible(false);

        float x = 273;
        float y = 530;

        levelGroup.clear();
        JsonProfile profile = core.getCurrentProfile();
        for (int i =0; i < levels.size; i++)
        {
            JsonLevel level = levels.get(i);
            boolean allowd = (i == 0) || profile.getPlayedLevelByName(null, levels.get(i - 1).getName()) != null && profile.getPlayedLevelByName(null, levels.get(i - 1).getName()).compleated;
            level.init(x, y, allowd, profile.getPlayedLevelByName(null, levels.get(i).getName()), star, emptyStar);
            levelGroup.addActor(level);
            x += 254;
            if (x > 1000) {x = 273; y -= 228;}
        }
    }

    public LevelSelectScreen()
    {
        core = Kernel.getInstance();

        viewport = new StretchViewport(Kernel.getViewportWidth(), Kernel.getViewportHeight());
        stage = new Stage(viewport);
        stageUI = new Stage(viewport);
        stageUI.addActor(core.getDialogBack());
        inputMultiplexer = new InputMultiplexer(stageUI, stage);
    }

    public void build()
    {


        initComponents();

        refresh();
    }

    public void setLevels(Array<JsonLevel> levels)
    {
        this.levels = levels;
    }

    private void initComponents() {
        background = new BackgroundActor(Preloader.manager.get(Preloader.levelselectbg));
        stage.addActor(background);
        levelsNails = new Image(Preloader.manager.get(Preloader.nails));
        levelsNails.setPosition(stage.getWidth() / 2 - levelsNails.getWidth() / 2, stage.getHeight() / 2 - levelsNails.getHeight() / 2 + 110);
        levelsNails.setTouchable(Touchable.disabled);

        levelGroup = new Group();

        stage.addActor(levelGroup);
        stage.addActor(levelsNails);

        final LevelSelectScreen me = this;

        //BACK button
        Button.ButtonStyle backStyle = new Button.ButtonStyle();
        backStyle.up = new SpriteDrawable(new Sprite(Preloader.manager.get(Preloader.icon_back)));
        Button back = new Button(backStyle);
        back.setPosition(50, stage.getHeight() - 50 - back.getHeight());

        back.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.playSound(Preloader.manager.get(Preloader.sound_click));
                core.showScreen(core.getCirclesSelectScreen());
            }
        });
        stage.addActor(back);

        AnimationDriver starAnimationDriver = new AnimationDriver(Preloader.manager.get(Preloader.star), 43, 41);

        Array<Integer> keyFrames = new Array<Integer>();
        keyFrames.add(4);
        keyFrames.add(5);
        keyFrames.add(6);
        keyFrames.add(7);
        keyFrames.add(8);
        keyFrames.add(7);
        keyFrames.add(6);
        keyFrames.add(5);
        keyFrames.add(4);
        star = starAnimationDriver.getAnimation(keyFrames, 0.1f);

        Array<Integer> emptyKeyFrames = new Array<Integer>();
        emptyKeyFrames.add(0);
        emptyKeyFrames.add(1);
        emptyKeyFrames.add(2);
        emptyKeyFrames.add(3);
        emptyKeyFrames.add(2);
        emptyKeyFrames.add(1);
        emptyStar = starAnimationDriver.getAnimation(emptyKeyFrames, 0.1f);

        Button.ButtonStyle buttonStyle = new Button.ButtonStyle();
        buttonStyle.up = new TextureRegionDrawable(new TextureRegion(Preloader.manager.get(Preloader.icon_levelup)));
        Button openSkillsButton = new Button(buttonStyle);
        openSkillsButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.getSkillsScreen().setBackScreen(core.getLevelSelectScreen());
                core.showScreen(core.getSkillsScreen());
            }
        });
        openSkillsButton.setPosition(stage.getWidth() - 173, stage.getHeight() - 175);
        stageUI.addActor(openSkillsButton);
    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

    public boolean switchToNextLevel() {
        String currentLevel = core.getCurrentLevel();

        for (int i = 0; i < levels.size; i++)
        {
            if (levels.get(i).getName().equals(currentLevel))
            {
                if (i + 1 < levels.size)
                {
                    core.setCurrentLevel(levels.get(i + 1).getName());
                    return true;
                }
            }
        }
        return false;
    }

    public void setBuilt(boolean built) {
        this.built = built;
    }
}
