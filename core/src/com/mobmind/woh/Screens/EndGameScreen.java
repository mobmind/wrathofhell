package com.mobmind.woh.Screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.mobmind.woh.Templates.TemplateScreen;

public class EndGameScreen implements TemplateScreen {

    @Override
    public Stage getStage() {
        return null;
    }

    @Override
    public Stage getStageUI() {
        return null;
    }

    @Override
	public void render(float v) {

	}

	@Override
	public void resize(int i, int i2) {

	}

	@Override
	public void show() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {

	}
}
