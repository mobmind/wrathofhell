package com.mobmind.woh.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobmind.woh.Actors.BackgroundActor;
import com.mobmind.woh.Actors.Menu.OpenSkillsButtonActor;
import com.mobmind.woh.Actors.Menu.PauseButtonActor;
import com.mobmind.woh.Actors.Menu.PostGameStats;
import com.mobmind.woh.Actors.MobActor;
import com.mobmind.woh.Actors.PlayerActor;
import com.mobmind.woh.Json.User.JsonPlayedLevel;
import com.mobmind.woh.Json.User.JsonProfile;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Listeners.Game.SwitchWeaponListener;
import com.mobmind.woh.Listeners.Game.TargetAtackListener;
import com.mobmind.woh.Templates.TemplateScreen;
import com.mobmind.woh.Tools.*;
import com.mobmind.woh.gui.ControlDialog;

public class GameScreen implements TemplateScreen {

    private Weapon armory;
    private Stage stage;
    private PlayerActor player;
    private Kernel core;
    private Stage stageUI;
    private StretchViewport viewport;
	private TargetAtackListener inputListner;
	private Group mobsGroup;

    //test
    public float circlex = 100;
    public float circley = 100;
    private ShapeRenderer shapeRenderer;
    private Group bottomEffectsGroup;
    private ControlDialog controlWindow;
    private boolean win;
    private InputMultiplexer inputMultiplexer;
    private BackgroundActor levelbg;
    private Image fogbg;
    private PostGameStats postGameStats;
    private Level level;
    private JsonProfile profile;
    private boolean built;
    private int totalKilled;
    private int totalScore;

    public Group getBottomEffectsGroup()
    {
        return bottomEffectsGroup;
    }
    public Stage getStage() {
        return stage;
    }
    public Stage getStageUI() {
        return stageUI;
    }
    public PlayerActor getPlayer() {
        return player;
    }
	public Group getMobsGroup(){ return mobsGroup; }

    public GameScreen() {
        core = Kernel.getInstance();
        armory = new Weapon();
        mobsGroup = new Group();
        viewport = new StretchViewport(
                Kernel.getViewportWidth(),
                Kernel.getViewportHeight()
        );
        stage = new Stage(viewport);
        stageUI = new Stage(viewport);
        stageUI.addActor(Kernel.getInstance().getDialogBack());
    }

    @Override
    public void show() {
        if (!Preloader.gameLoaded) {
            core.getLoadingScreen().setNextScreen(this);
            core.showScreen(core.getLoadingScreen());
            Preloader.loadGameData();
            return;
        }
        if (built)
        {
            refresh();
        }else{
            stage.clear();
            stageUI.clear();
            build();
            built = true;
        }
    }

    public void refresh()
    {
        profile = core.getCurrentProfile();
        Gdx.input.setInputProcessor(inputMultiplexer);
        stageUI.addActor(Kernel.getInstance().getDialogBack());

        player.refresh();
        for (Actor mob : mobsGroup.getChildren())
        {
            ((MobActor)mob).dispose();
        }
        mobsGroup.clear();
        stage.clear();
        stage.addListener(inputListner);
        stage.addActor(levelbg);
        stage.addActor(player);
        stage.addActor(bottomEffectsGroup);
        stage.addActor(mobsGroup);
        stage.addActor(fogbg);

        level = new Level(core.getCurrentLevel());
        totalKilled = 0;
        totalScore = 0;
        stage.addAction(level);

        hidePostGameStats();
        unpause();
    }

    public void hidePostGameStats() {
        postGameStats.setVisible(false);
    }


    private void build()
    {

        initComponents();
        inputMultiplexer = new InputMultiplexer(new APMCounter(), stageUI, stage);

//	    new GameUI();

        stage.addListener(new SwitchWeaponListener());


        inputListner = new TargetAtackListener(this);

        //test
        shapeRenderer = new ShapeRenderer();
        shapeRenderer = new ShapeRenderer();

        refresh();
    }

    private void initComponents() {
        Preloader.lightingKick = (new AnimationDriver(Preloader.manager.get(Preloader.lightingKickAsset).textures)).getAnimation(1, 11, 0.15f);
        levelbg = new BackgroundActor(Preloader.manager.get(Preloader.levelbg));
        player = new PlayerActor();
        bottomEffectsGroup = new Group();

        fogbg = new Image(Preloader.manager.get(Preloader.fogbg));

        OpenSkillsButtonActor openSkillsButtonActor = new OpenSkillsButtonActor();

        PauseButtonActor pauseButtonActor = new PauseButtonActor();

        stageUI.addActor(openSkillsButtonActor);
        stageUI.addActor(pauseButtonActor);

        controlWindow = new ControlDialog();


        stageUI.addActor(controlWindow);
        postGameStats = new PostGameStats(this);
    }

    @Override
    public void render(float v) {
        if (!Gdx.input.isTouched()) {
            inputListner.setIsTouched(false);
        }
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        if (!Kernel.paused) {
            stage.act(v);
        } else {
            player.act(v);
        }
        stage.draw();

        /*shapeRenderer.setProjectionMatrix(stage.getCamera().combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(1, 1, 1, 1);
        shapeRenderer.circle(circlex, circley, 50);
        shapeRenderer.end();*/
        if (!Kernel.paused) {
            stageUI.act(v);
        }else{
            postGameStats.act(v);
        }
        stageUI.draw();
        Table.drawDebug(stage);
        Table.drawDebug(stageUI);
    }

    @Override
    public void resize(int i, int i2) {
        viewport.update(i, i2);
    }

    @Override
    public void dispose() {
	    stage.dispose();
    }

    @Override
    public void hide() {}

    @Override
    public void pause() {
        if (!postGameStats.isVisible()) {
            Kernel.paused = true;
            controlWindow.setVisible(true);
        }
    }

    @Override
    public void resume() {

    }

    public void setWeaponRange(float weaponRange) {
        for (Actor mob : mobsGroup.getChildren())
        {
            ((MobActor)mob).recalcWeaponRange(weaponRange);
        }
    }

    public void showPostGameStats()
    {
        postGameStats.setVisible(true);
    }

    public void gameOver() {
        Kernel.paused = true;
    }

    public void setWin(boolean win) {
        if (win)
        {
            int starsCount = profile.setLevelProgress(core.getCurrentLevel(), true, totalKilled, totalScore);
            postGameStats.setStars(starsCount);
            core.saveCurrentProfile();
        }
        this.win = win;
    }

    public void restart() {
        refresh();
    }

    public boolean isVictory() {
        return win;
    }

    public void addScore(int score)
    {
        totalScore += score;
    }

    public void onMobDeath(MobActor mobActor, boolean killed) {
        if (killed) { totalKilled++; }
    }

    public boolean checkVictory()
    {
        if (!mobsGroup.hasChildren())
        {
            winGame();
            return true;
        }
        return false;
    }

    private void winGame()
    {
        setWin(true);
        gameOver();
        showPostGameStats();
    }

    public void unpause() {
        Kernel.paused = false;
        if (!postGameStats.isVisible()) {
            controlWindow.setVisible(false);
        }
    }

    public void playNextLevel() {
        if (core.getLevelSelectScreen().switchToNextLevel())
        {
            refresh();
        }else{
            core.showScreen(core.getLevelSelectScreen());
        }
    }

    public void setBuilt(boolean built) {
        this.built = built;
    }

    public int getTotalKilled() {
        return totalKilled;
    }

    public int getTotalScore() {
        return totalScore;
    }
}
