package com.mobmind.woh.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobmind.woh.Actors.BackgroundActor;
import com.mobmind.woh.Json.JsonCircles;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Templates.TemplateScreen;
import com.mobmind.woh.Tools.DialogBack;
import com.mobmind.woh.Tools.Preloader;
import com.mobmind.woh.Tools.SettingsMenu;
import com.mobmind.woh.gui.CirclesMetro;


/**
 * Created by seedofpanic on 26.07.2014.
 */
public class CirclesSelectScreen implements TemplateScreen {

    private Stage stage;
    private Stage stageUI;
    private Kernel core;
    private StretchViewport viewport;
    private BackgroundActor background;
    public JsonCircles circles;
    private Json json;
    private CirclesMetro circlesMetro;
    private InputMultiplexer inputMultiplexer;
    private boolean built;

    @Override
    public Stage getStage() {
        return stage;
    }

    @Override
    public Stage getStageUI() {
        return stageUI;
    }

    @Override
    public void render(float v) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT | GL30.GL_DEPTH_BUFFER_BIT);

        stage.act(v);
        stage.draw();

        stageUI.act(v);
        stageUI.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update();
    }

    @Override
    public void show() {
        if (!Preloader.menuLoaded) {
            core.getLoadingScreen().setNextScreen(this);
            core.showScreen(core.getLoadingScreen());
            Preloader.loadMenuData();
            return;
        }
        if (built)
        {
            refresh();
        }else{
            stage.clear();
            stageUI.clear();
            build();
            built = true;
        }
    }

    private void refresh(){
        Gdx.input.setInputProcessor(inputMultiplexer);
        stageUI.addActor(Kernel.getInstance().getDialogBack());
    }

    public CirclesSelectScreen()
    {
        core = Kernel.getInstance();
        viewport = new StretchViewport(Kernel.getViewportWidth(), Kernel.getViewportHeight());
        stage = new Stage(viewport);
        json = core.getJson();
        stageUI = new Stage(viewport);
        stageUI.addActor(core.getDialogBack());
        inputMultiplexer = new InputMultiplexer(stageUI, stage);

        circles = json.fromJson(JsonCircles.class, Gdx.files.internal("Json/circles.json"));
    }

    private void build(){

        initComponents();

        refresh();
    }

    private void initComponents() {
        final CirclesSelectScreen me = this;
        background = new BackgroundActor(Preloader.manager.get(Preloader.levelselectbg));
        stage.addActor(background);
        circlesMetro = new CirclesMetro();
        circles.init(circlesMetro);
        circlesMetro.setWidth(stage.getWidth());
        circlesMetro.setHeight(stage.getHeight());
        circlesMetro.init();
        stage.addActor(circlesMetro);

        //buttons

        SettingsMenu settingsMenu = new SettingsMenu(stage.getWidth() - 50, stage.getHeight() - 50, null);


        //BACK button
        Button.ButtonStyle backStyle = new Button.ButtonStyle();
        backStyle.up = new SpriteDrawable(new Sprite(Preloader.manager.get(Preloader.icon_back)));
        Button back = new Button(backStyle);
        back.setPosition(50, stage.getHeight() - 50 - back.getHeight());

        back.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.playSound(Preloader.manager.get(Preloader.sound_click));
                core.showScreen(core.getMenuScreen());
            }
        });
        stage.addActor(back);




    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

    public void setBuilt(boolean built) {
        this.built = built;
    }
}
