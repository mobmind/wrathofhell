package com.mobmind.woh.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobmind.woh.Actors.Menu.LoadLine;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Templates.TemplateScreen;
import com.mobmind.woh.Tools.Pallet;
import com.mobmind.woh.Tools.Preloader;
import com.mobmind.woh.gui.CentredLabel;

import javax.swing.*;

/**
 * Created by seedofpanic on 17.08.2014.
 */
public class LoadingScreen implements TemplateScreen {
    private final Kernel core;
    private final StretchViewport viewport;
    //private final Timer renderTimer;
    private Stage stage;
    private TemplateScreen nextScreen;
    private boolean fontLoaded = false;
    private boolean firstFrameSkipped;
    private String message;
    private boolean built = false;
    private LoadLine loadLine;
    private boolean lastRender;

    public void setMessage(String message)
    {
        this.message = message;
    }

    public void setNextScreen(TemplateScreen nextScreen)
    {
        this.nextScreen = nextScreen;
    }

    public LoadingScreen()
    {
        core = Kernel.getInstance();
        viewport = new StretchViewport(
                Kernel.getViewportWidth(),
                Kernel.getViewportHeight()
        );
        stage = new Stage(viewport);



    }

    @Override
    public Stage getStage() {
        return stage;
    }

    @Override
    public Stage getStageUI() {
        return null;
    }

    @Override
    public void render(float delta) {

        if (lastRender)
        {
            core.showScreen(nextScreen);
        }

        stage.act();
        Gdx.gl.glClearColor(Pallet.LoadingBackBrown.r, Pallet.LoadingBackBrown.g, Pallet.LoadingBackBrown.b, Pallet.LoadingBackBrown.a);
        Gdx.gl.glClear(Gdx.gl20.GL_COLOR_BUFFER_BIT);
        stage.draw();

        if (firstFrameSkipped) {
            if (Preloader.manager.update(500)) {
                lastRender = true;
            }
            loadLine.setWidth(Preloader.manager.getProgress() * 855);
        }else{
            firstFrameSkipped = true;
        }
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(null);
        firstFrameSkipped = false;
        if (!built) {
            Image loadSheet = new Image(Preloader.manager.get(Preloader.loadSheet));
            loadSheet.setPosition(stage.getWidth() / 2 - loadSheet.getWidth() / 2, stage.getHeight() - loadSheet.getHeight());
            stage.addActor(loadSheet);

            Label.LabelStyle labelStyle = new Label.LabelStyle();
            labelStyle.font = Preloader.mainFont22;
            labelStyle.fontColor = Pallet.LoadingText;
            Label loadingText  = new Label("Once upon a time a strong Mage of a strong kingdom found a way to the deeps of Hell. Thinking of becoming famous he headed there to investigate...", labelStyle);
            loadingText.setAlignment(Align.center);
            loadingText.setWrap(true);
            loadingText.setWidth(loadSheet.getWidth() - 160);
            loadingText.setPosition(stage.getWidth() / 2 - loadingText.getWidth() / 2, stage.getHeight() - 250);
            stage.addActor(loadingText);

            Label.LabelStyle style = new Label.LabelStyle();
            style.font = Preloader.mainFont30;
            style.fontColor = Pallet.LoadingMessage;
            CentredLabel pleaseWaitLabel = new CentredLabel("Loading, please wait...", style);
            pleaseWaitLabel.setPosition(stage.getWidth() / 2, 50);
            stage.addActor(pleaseWaitLabel);
            built = true;

            Image loadBar = new Image(Preloader.manager.get(Preloader.loadBar));
            loadBar.setPosition(stage.getWidth() / 2 - loadBar.getWidth() / 2, 120);

            loadLine = new LoadLine(Preloader.manager.get(Preloader.loadLine), loadBar.getX() + 19, loadBar.getY() + 11, 0, 38, 0, 0);

            stage.addActor(loadLine);
            stage.addActor(loadBar);


        }
        loadLine.setWidth(0);
        lastRender = false;
    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
