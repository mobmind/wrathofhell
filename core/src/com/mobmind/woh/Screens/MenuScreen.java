package com.mobmind.woh.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobmind.woh.Actors.BackgroundActor;
import com.mobmind.woh.Actors.Menu.MenuPreAnimation;
import com.mobmind.woh.Actors.Menu.MessageBox;
import com.mobmind.woh.Actors.Menu.StartBtn;
import com.mobmind.woh.Json.User.JsonProfile;
import com.mobmind.woh.Kernel;
import com.mobmind.woh.Listeners.Menu.TapListener;
import com.mobmind.woh.Templates.TemplateScreen;
import com.mobmind.woh.Tools.AnimButton;
import com.mobmind.woh.Tools.Pallet;
import com.mobmind.woh.Tools.Preloader;
import com.mobmind.woh.gui.*;
import com.mobmind.woh.gui.Dialog;

public class MenuScreen implements TemplateScreen {

	private StretchViewport viewport;
	private Stage stage;
	private Kernel core;
    private BackgroundActor background;
    private StartBtn startBtn;
    private Group selectProfileGroup;
    private Group statGameGroup;
    private Group currentMenuGroup;
    private Group changeProfileGroup;
    public Array<Integer> animBtnKeyFrames;
    private Group profileMenuPage;
    private ListBox<JsonProfile> profilesList;
    private MenuPreAnimation menuPreAnimation;
    private Dialog deleteProfileDialog;
    private CentredLabel profileNameText;
    private InputDialog newProfileDialog;
    private Stage stageUI;
    private boolean built;
    private InputMultiplexer inputMultiplexer;
    private MessageBox messageBox;
    private InputDialog changeNameDialog;

    public Stage getStage(){ return stage; }
	public Stage getStageUI(){ return stageUI; }

    public MenuScreen()
    {
        core = Kernel.getInstance();
        viewport = new StretchViewport(Kernel.getViewportWidth(), Kernel.getViewportHeight());
        stage = new Stage(viewport);
        stageUI = new Stage(viewport);
        stageUI.addActor(core.getDialogBack());
        inputMultiplexer = new InputMultiplexer(stageUI, stage);
    }

	@Override
	public void show() {
        if (!Preloader.menuLoaded) {
            core.getLoadingScreen().setNextScreen(this);
            core.showScreen(core.getLoadingScreen());
            Preloader.loadMenuData();
            return;
        }
		if (built)
        {
            refresh();
        }else{
            stage.clear();
            stageUI.clear();
            build();
            built = true;
            core.loadGame();
        }
	}

    private void refresh()
    {
        Gdx.input.setInputProcessor(inputMultiplexer);
        stageUI.addActor(Kernel.getInstance().getDialogBack());
    }

    private void build()
    {

        animBtnKeyFrames = Preloader.textKeyFrames;


        initComponents();

        refresh();
    }

    @Override
    public void render(float v) {

	    stage.act(v);
	    stage.draw();

        stageUI.act();
        stageUI.draw();
    }

    @Override
    public void resize(int i, int i2) {
		viewport.update();
    }

    @Override
    public void hide() {}

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void dispose() { stage.dispose(); }

    public void switchMenuPage(Group menuGroup)
    {
        if (currentMenuGroup != null) { currentMenuGroup.setVisible(false); }
        currentMenuGroup = menuGroup;
        currentMenuGroup.setVisible(true);
    }

    public void initMenu() {
        startBtn.setVisible(false);
        menuPreAnimation.setVisible(true);
    }

    private void initComponents()
    {
        background = new BackgroundActor(Preloader.manager.get(Preloader.mainMenuBG));
        stage.addActor(background);
        background.addListener(new TapListener());
        startBtn = new StartBtn();

        menuPreAnimation = new MenuPreAnimation();
        menuPreAnimation.setVisible(false);
        selectProfileGroup = new Group();
        selectProfileGroup.setVisible(false);

        Array<Integer> headerKeyFrames = new Array<Integer>();
        for (int i = 1; i < 22; i++)
        {
            headerKeyFrames.add(i);
        }

        AnimButton newProfile = new AnimButton(Preloader.manager.get(Preloader.newProfile), 420, 345, 456, 60, animBtnKeyFrames, 0.04f);
        newProfile.setSoundEffect(Preloader.manager.get(Preloader.sound_ignite));
        AnimButton select = new AnimButton(Preloader.manager.get(Preloader.selectBtn), 534, 276, 232, 60, animBtnKeyFrames, 0.04f);
        select.setSoundEffect(Preloader.manager.get(Preloader.sound_ignite));
        AnimButton change = new AnimButton(Preloader.manager.get(Preloader.changeBtn), 519, 208, 261, 60, animBtnKeyFrames, 0.04f);
        change.setSoundEffect(Preloader.manager.get(Preloader.sound_ignite));
        AnimButton delete = new AnimButton(Preloader.manager.get(Preloader.deleteBtn), 518, 140, 256, 60, animBtnKeyFrames, 0.04f);
        delete.setSoundEffect(Preloader.manager.get(Preloader.sound_ignite));


        select.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.showScreen(core.getCirclesSelectScreen());
            }
        });
        newProfile.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                newProfileDialog.setVisible(true);
                newProfileDialog.setInputText("");
                newProfileDialog.setCloseable(true);
            }
        });
        change.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                changeNameDialog.setVisible(true);
                changeNameDialog.setInputText(core.getCurrentProfile().name);
                changeNameDialog.setCloseable(true);
            }
        });
        delete.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.getMenuScreen().showDeleteDialog();
            }
        });

        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = Preloader.mainFont22;
        labelStyle.fontColor = Pallet.Brown;
        profileNameText = new CentredLabel("", labelStyle);
        profileNameText.setPosition(650, 470);
        profileNameText.setText("");
        profileNameText.setCenterY(true);
        profileNameText.addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                core.playSound(Preloader.manager.get(Preloader.sound_click));
                core.getMenuScreen().initChangeProfile();
                return true;
            }
        });

        selectProfileGroup.addActor(profileNameText);
        //selectProfileGroup.addActor(header);
        selectProfileGroup.addActor(newProfile);
        selectProfileGroup.addActor(select);
        selectProfileGroup.addActor(change);
        selectProfileGroup.addActor(delete);

        stageUI.addActor(selectProfileGroup);

        ListBox.ListBoxStyle style = new ListBox.ListBoxStyle();
        style.listStyle = new List.ListStyle();
        style.listStyle.selection = new BaseDrawable();
        style.listStyle.fontColorUnselected = Color.BLACK;
        style.listStyle.fontColorSelected = Color.TEAL;
        style.listStyle.font = Preloader.mainFont22;
        style.scrollPaneStyle = new ScrollPane.ScrollPaneStyle();

        profilesList = new ListBox<JsonProfile>(style){
            @Override
            public void setVisible(boolean visible) {
                Kernel.getInstance().getDialogBack().setVisible(visible);
                toFront();
                super.setVisible(visible);
            }
        };

        profilesList.setBounds(265, 300, 750, 300);


        profilesList.addChangListener(new ChangeListener() {
                                          @Override
                                          public void changed(ChangeEvent event, Actor actor) {
                                              if (((List)actor).getSelected() == null) {return;}
                                              core.playSound(Preloader.manager.get(Preloader.sound_click));
                                              String name = ((List)actor).getSelected().toString();
                                              core.setCurProfile(name);
                                              profilesList.setVisible(false);
                                              profileNameText.setText(core.getCurrentProfile().name);
                                          }
                                      }
        );
        profilesList.setVisible(false);
        stageUI.addActor(profilesList);

        //Delete Dialog
        deleteProfileDialog = new Dialog(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                core.deletePrfile();
                ((MenuScreen)core.getMenuScreen()).hideDeleteDialog();
            }
        }, new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                ((MenuScreen)core.getMenuScreen()).hideDeleteDialog();
            }
        });
        deleteProfileDialog.setPosition(640, 413);
        deleteProfileDialog.setVisible(false);
        stageUI.addActor(deleteProfileDialog);

        //Input Dialog
        newProfileDialog = new InputDialog("Type in your first profile \n name please", new ChangeListener(){
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                InputDialog inputDialog = (InputDialog)actor.getParent();
                String name = inputDialog.getInputText().getText();
                if (name.length() > 0) {
                    if (core.validateProfileName(name))
                    {
                        core.newProfile(name);
                        newProfileDialog.setVisible(false);
                    }else{
                        showPlayerExistsMessage();
                    }
                }
            }
        });
        newProfileDialog.setPosition(640, 470);
        newProfileDialog.setVisible(false);
        stageUI.addActor(newProfileDialog);

        // Change name Input Dialog
        changeNameDialog = new InputDialog("Enter your name please", new ChangeListener(){
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                InputDialog inputDialog = (InputDialog)actor.getParent();
                String name = inputDialog.getInputText().getText();
                if (name.length() > 0) {
                    if (core.validateProfileName(name))
                    {
                        core.renameProfile(name);
                        changeNameDialog.setVisible(false);
                    }else{
                        showPlayerExistsMessage();
                    }
                }
            }
        });
        changeNameDialog.setPosition(640, 470);
        changeNameDialog.setVisible(false);
        stageUI.addActor(changeNameDialog);

        messageBox = new MessageBox(new ChangeListener(){

            @Override
            public void changed(ChangeEvent event, Actor actor) {
                messageBox.setVisible(false);
            }
        });
        stageUI.addActor(messageBox);
    }

    private void showPlayerExistsMessage() {
        messageBox.setMessage("Profile with such name \n already exists!");
        messageBox.setVisible(true);
    }

    public void initChangeProfile()
    {
        profilesList.setVisible(true);
    }

    public void initNewProfile()
    {

    }

    public void setVisibleProfileGroup()
    {
        selectProfileGroup.setVisible(true);
    }

    public StartBtn getStartBtn() {
        return startBtn;
    }

    public BackgroundActor getBackground() {
        return background;
    }

    public Group getSelectProfileGroup() {
        return selectProfileGroup;
    }

    public void showDeleteDialog() {
        deleteProfileDialog.setVisible(true);
    }

    public void hideDeleteDialog() {
        deleteProfileDialog.setVisible(false);
    }

    public void updateCurProfile()
    {
        profileNameText.setText(core.getCurrentProfile().name);
        profilesList.setItems(core.getProfilesList(), core.getCurrentProfile());
        deleteProfileDialog.setMsg("Are you sure, you want to delete profile " + core.getCurrentProfile().name + "?");
    }

    public void askNewProfile() {
        newProfileDialog.setVisible(true);
        newProfileDialog.setCloseable(false);
    }

    public void setBuilt(boolean built) {
        this.built = built;
    }
}
