package com.mobmind.woh.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mobmind.woh.Kernel;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "WrathOfHell";

		cfg.width = 1280;
		cfg.height = 800;
		cfg.useGL30 = false;
		cfg.resizable = true;
		cfg.fullscreen = false;
//		cfg.fullscreen = true;
		cfg.vSyncEnabled = true;
		new LwjglApplication(Kernel.getInstance(), cfg);
	}
}


